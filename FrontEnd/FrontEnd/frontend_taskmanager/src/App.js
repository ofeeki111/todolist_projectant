import "./App.css";
import Layout from "./Components/Layout";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import SingleTaskPage from "./Components/TaskPage/SingleTaskPage";
import Task from "./Components/Task";
import { ReactQueryDevtools } from "react-query/devtools";
import ContactsPage from "./Components/ContactsPage/ContactsPage";
import LoginPage from "./Components/LoginPage/LoginPage";
import RegisterPage from "./Components/RegisterPage/RegisterPage";
import { createContext, useEffect } from "react";
import TagsPage from "./Components/TagsPage/TagsPage";

export const userContext = createContext();
function App() {
  const queryClient = new QueryClient();


  return (
    <div className="App">
      <userContext.Provider
        value={JSON.parse(localStorage.getItem("currentUser"))}
      >
        <QueryClientProvider client={queryClient}>
          {localStorage.getItem("currentUser") === null ? (
            <Routes>
              <Route path="/" element={<LoginPage />} />
              <Route path="/register" element={<RegisterPage />} />
            </Routes>
          ) : (
            <Routes>
              <Route path="/register" element={<RegisterPage />} />
              <Route path="/" currentSession element={<Layout />} />
              <Route path="/contacts" element={<ContactsPage />} />
              <Route path="/tags" element={<TagsPage />} />
              <Route path="/task/:taskId" element={<SingleTaskPage />} />
            </Routes>
          )}
          <ReactQueryDevtools initialIsOpen={false} />
        </QueryClientProvider>
      </userContext.Provider>
    </div>
  );
}

export default App;
