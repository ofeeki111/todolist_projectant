import React, { useState, useEffect, useContext } from "react";
import { userContext } from "../App";
import { Grid, Card, Alert, AlertTitle, IconButton } from "@mui/material";
import Task from "./Task";
import Subtask from "./Subtask";
import Stack from "@mui/material/Stack";
import { FallingLines } from "react-loader-spinner";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { deleteTag, fetchTasksHandler, getTags, postTask } from "../Services/Api";
import moment from "../../node_modules/moment/moment";
import { orderTasksById } from "../Services/SortService";
import { Backdrop, Box, Checkbox, Chip, Divider, Modal, TextField, Typography } from "../../node_modules/@mui/material/index";
import { getAllTasksForToday } from "../Services/tasksService";
import { Add, AddAlarm, AddCircle, AltRoute, DateRange, DateRangeOutlined } from "../../node_modules/@mui/icons-material/index";
import TaskAddForm from "./TaskAddForm";
import Navbar from "./Navbar";
import AddTag from "./TagsPage/AddTag";
function Layout(props) {
  const user = useContext(userContext);
  const todayDate = new Date();
  const { data, status } = useQuery("tasks", () => fetchTasksHandler(user.session_id, user.username), {});
  const { data: tags, status: tagsStatus } = useQuery("tags", () => getTags(user.session_id, user.username), {});
  const [showTagForm, setShowTagForm] = useState(false);
  const [showTaskForm, setShowTaskForm] = useState(false);
  const [plannedForm, setPlannedForm] = useState(true);
  const [showAllTasks, setShowAllTasks] = useState(true);
  const [dateSelected, setDateSelected] = useState(todayDate);
  const [tasks, setTasks] = useState([]);
  const queryClient = new useQueryClient();

  const { mutate: deleteTagMutation, isLoading } = useMutation(deleteTag, {
    onSuccess: () => {
      queryClient.invalidateQueries("tags");
      queryClient.invalidateQueries("tasks");
    },
  });
  const [isError, setError] = useState({ isErr: false, content: "error" });

  useEffect(() => {
    document.title = `${user.username}'s tasks`;
  });
  useEffect(() => {
    setTasks(data);
    const timeId = setTimeout(() => {
      // After 3 seconds set the show value to false
      setError({ isErr: false, content: "" });
    }, 3000);

    return () => {
      clearTimeout(timeId);
    };
  });

  const deleteTagHandler = (tag_id) => {
    const deleteParams = { username: user.username, tag_id: tag_id };
    console.log("delete " + "user " + user.username + "tag " + tag_id);
    deleteTagMutation(deleteParams);
  };

  return (
    <div>
      <Navbar></Navbar>
      <div style={{ marginTop: 50 }}>
        <Typography variant="h1" color="white">
          Tasks
        </Typography>
        {showTagForm ? (
          <Modal
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            open={showTagForm}
            onClose={() => setShowTagForm(!showTagForm)}
          >
            <Box alignItems="center">
              <AddTag sx={{ color: "#fff" }} />
            </Box>
          </Modal>
        ) : null}
        {showTaskForm ? (
          <Modal
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            open={showTaskForm}
            onClose={() => setShowTaskForm(!showTaskForm)}
          >
            <Box alignItems="center">
              <TaskAddForm {...props} setError={setError} isPlanned={plannedForm} sx={{ color: "#fff" }} />
            </Box>
          </Modal>
        ) : null}
        <Grid container direction="row" spacing={2} justifyContent="center">
          {isError.isErr ? (
            <Alert variant="standard" severity="error" className="animate__shakeX" color="error">
              {isError.content}{" "}
            </Alert>
          ) : (
            <></>
          )}
        </Grid>
        <Grid item xs={12} justifyContent="center" direction="column">
          {status === "error" && <p>Error fetching data</p>}

          {status === "loading" && <FallingLines color="#fff" width="150" visible={true} ariaLabel="falling-lines-loading" />}
        </Grid>

        {status === "success" && (
          <div>
            {tags?.map((tag) => (
              <IconButton onClick={() => deleteTagHandler(tag.id)}>
                <Typography
                  color="white"
                  style={{
                    backgroundColor: tag.colorOfTag,
                    borderRadius: "10%",
                    padding: "4px",
                  }}
                >
                  {tag.name}
                </Typography>
              </IconButton>
            ))}

            <IconButton
              onClick={() => {
                setShowTagForm(true);
              }}
            >
              <Add sx={{ color: "white" }}></Add>
            </IconButton>
            <Grid sx={{ marginTop: 5 }} justifyContent="center">
              <Grid item container xs={12} justifyContent="center" spacing={"5%"}>
                <Grid item xs={{ order: 1 }} justifyContent="center">
                  <Typography variant="h2" textAlign="left" sx={{ color: "#b6b6b6 " }}>
                    <strong>Scheduled</strong> tasks
                    <Divider orientation="horizontal" sx={{ bgcolor: "#b6b6b6" }} />
                  </Typography>
                  <TextField
                    onChange={(e) => setDateSelected(e.target.value)}
                    className="inputTextField"
                    fullWidth
                    margin="normal"
                    id="date"
                    type="date"
                    defaultValue={moment(dateSelected).format("YYYY-MM-DD")}
                    sx={{ width: 400 }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <Checkbox icon={<DateRangeOutlined sx={{ fontSize: 60 }} />} checkedIcon={<DateRange sx={{ fontSize: 60 }} />} onChange={(e) => setShowAllTasks(!e.target.checked)}></Checkbox>
                  <Stack justifyContent="center">
                    {data.length > 0 &&
                      getAllTasksForToday(dateSelected, data, false, showAllTasks)?.map((task) => {
                        return (
                          <Task key={task.task_id} setError={setError} {...task}>
                            {" "}
                          </Task>
                        );
                      })}
                  </Stack>
                  <IconButton
                    style={{ color: "#b2ffb2" }}
                    onClick={() => {
                      setPlannedForm(false);
                      setShowTaskForm(true);
                    }}
                  >
                    <AddCircle sx={{ fontSize: 50 }} />
                  </IconButton>
                </Grid>
                <Grid item xs={{ order: 2 }}>
                  <Typography textAlign="left" variant="h2" sx={{ color: "#b6b6b6" }}>
                    <strong>Planned</strong> for today
                  </Typography>
                  <Divider orientation="horizontal" sx={{ bgcolor: "#b6b6b6" }} />
                  <Stack alignItems="top">
                    {getAllTasksForToday(todayDate, data, true, true)?.map((task) => {
                      return (
                        <Task key={task.task_id} setError={setError} {...task}>
                          {" "}
                        </Task>
                      );
                    })}
                  </Stack>
                  <IconButton
                    style={{ color: "#b2ffb2" }}
                    onClick={() => {
                      setPlannedForm(true);
                      setShowTaskForm(true);
                    }}
                  >
                    <AddCircle sx={{ fontSize: 50 }} />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </div>
        )}
      </div>
    </div>
  );
}

export default Layout;
