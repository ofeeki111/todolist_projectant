import * as React from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";
import { useState } from "react";
import { AppBar, Drawer } from "../../node_modules/@mui/material/index";
import {
  Close,
  ContactMail,
  DomainVerification,
  DomainVerificationOutlined,
  ExitToApp,
  Login,
  Tag,
} from "../../node_modules/@mui/icons-material/index";
import {
  Link,
  Navigate,
  useNavigate,
} from "../../node_modules/react-router-dom/index";
import { mt } from "date-fns/locale";
const Navbar = () => {
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();

  const handleSignOut = () => {
    localStorage.removeItem("currentUser");
    navigate("/");
    navigate(0);
  };

  return !open ? (
    <IconButton
      style={{
        display: "flex",
        justifyContent: "left",
        color: "white",
      }}
      onClick={() => {
        setOpen(true);
      }}
    >
      <MenuIcon style={{ fontSize: 40 }}></MenuIcon>
    </IconButton>
  ) : (
    <AppBar
      style={{
        display: "flex",
      }}
    >
      <Drawer
        hideBackdrop
        anchor="left"
        xs={{ anchor: "top" }}
        open={open}
        permanent
        PaperProps={{
          sx: {
            backgroundColor: "#313131",
            color: "#fffff",
          },
        }}
      >
        <List>
          <ListItem
            disablePadding
            sx={{ display: "block", marginTop: 1, marginBottom: 4 }}
            onClick={() => {
              setOpen(false);
            }}
          >
            <ListItemButton
              style={{ fontSize: 40 }}
              sx={{
                minHeight: 48,
                justifyContent: "center",
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  color: "white",
                  minWidth: 0,
                  mr: "auto",
                  justifyContent: "center",
                }}
              >
                <Close sx={{ fontSize: 40 }}></Close>
              </ListItemIcon>
            </ListItemButton>
          </ListItem>

          <Link to="/">
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: "center",
                  px: 2.5,
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: "auto",
                    justifyContent: "center",
                  }}
                >
                  <DomainVerification sx={{ fontSize: 40 }} />
                </ListItemIcon>
              </ListItemButton>
            </ListItem>
          </Link>
          <Link to="/contacts">
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: "center",
                  px: 2.5,
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: "auto",
                    justifyContent: "center",
                  }}
                >
                  <ContactMail sx={{ fontSize: 40 }} />
                </ListItemIcon>
              </ListItemButton>
            </ListItem>
          </Link>

          <Link to="/tags">
            <ListItem disablePadding sx={{ display: "block" }}>
              <ListItemButton
                sx={{
                  minHeight: 48,
                  justifyContent: "center",
                  px: 2.5,
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: "auto",
                    justifyContent: "center",
                  }}
                >
                  <Tag sx={{ fontSize: 40 }} />
                </ListItemIcon>
              </ListItemButton>
            </ListItem>
          </Link>


          <ListItem disablePadding sx={{ pt: "100%" }} onClick={handleSignOut}>
            <ListItemButton
              sx={{
                minHeight: 48,
                justifyContent: "center",
                px: 2.5,
              }}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: "auto",
                  justifyContent: "center",
                }}
              >
                <ExitToApp sx={{ fontSize: 40 }} />
              </ListItemIcon>
            </ListItemButton>
          </ListItem>
        </List>
      </Drawer>
    </AppBar>
  );
};

export default Navbar;
