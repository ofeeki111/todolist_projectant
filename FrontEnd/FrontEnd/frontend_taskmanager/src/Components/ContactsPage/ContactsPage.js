import React, { useContext } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { useEffect, useState } from "react";
import {
  getContacts,
  deleteContact,
  fetchTasksHandler,
  assignContact,
} from "../../Services/Api";
import Navbar from "../Navbar";
import {
  Avatar,
  Box,
  Container,
  Grid,
  IconButton,
  MenuItem,
  Modal,
  Select,
  SvgIcon,
  Typography,
} from "../../../node_modules/@mui/material/index";
import { useTheme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";
import { format } from "date-fns";
import { Link } from "../../../node_modules/react-router-dom/index";
import {
  AddBox,
  AddCircle,
  AddTaskSharp,
  Assignment,
  Block,
  Delete,
  Edit,
  PostAdd,
} from "../../../node_modules/@mui/icons-material/index";
import ContactAddForm from "./ContactAddForm";
import { FallingLines } from "../../../node_modules/react-loader-spinner/dist/index";
import { userContext } from "../../App";

const useStyles = makeStyles((theme) => ({
  table: {},
  tableContainer: {
    borderRadius: 15,
    margin: "10px 10px",
  },
  tableHeaderCell: {
    fontWeight: "bold",
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
  },
  tableBodyCell: {},
  TableRow: {
    backgroundColor: "#444444",
    "&:hover": {
      backgroundColor: "#505050",
    },
  },
  avatar: {
    backgroundColor: theme.palette.primary.light,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
  },
  name: {
    fontWeight: "bold",
    textAlign: "left",
    color: "#ffffff",
  },
  status: {
    fontWeight: "bold",
    fontSize: "0.75rem",
    color: "white",
    backgroundColor: "grey",
    borderRadius: 8,
    padding: "3px 10px",
    display: "inline-block",
  },
}));

const ContactsPage = () => {
  const user = useContext(userContext);
  const queryClient = new useQueryClient();
  const classes = useStyles();
  const theme = useTheme();

  const [showContactForm, setContactForm] = useState(false);

  const {
    data: tasks,
    status: taskStatus,
    isLoading: isTaskLoading,
    isError: isTasksError,
  } = useQuery(
    "tasks",
    () => fetchTasksHandler(user.session_id, user.username),
    {}
  );

  const {
    data: contacts,
    status,
    isLoading: isContactsLoading,
    isError,
  } = useQuery(
    ["contacts"],
    () => getContacts(user.session_id, user.username),
    {}
  );

  const { mutate: deleteContactMutation } = useMutation(deleteContact, {
    onSuccess: () => {
      queryClient.invalidateQueries("contacts");
    },
  });

  const deleteContactHandler = (contact_id) => {
    deleteContactMutation(contact_id);
  };
  const { mutate: addContact } = useMutation(assignContact, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks", "contacts");
    },
  });

  const assignContactHandler = (contact_id, task_id) => {
    const postArgs = { task_id: task_id, contact_id: contact_id };
    addContact(postArgs);
  };

  useEffect(() => {
    if (status === "success") {
      const newTasks = tasks?.map((task) => {
        if (!task.contactsOfTask.some((Ctask) => Ctask.id === task.id))
          return { ...task, disabled: false };
        else {
          return { ...task, disabled: true };
        }
      });
    }
  }, [status, tasks]);
  useEffect(() => {
    document.title = `${user.username}'s contacts`;
  });
  return (
    <div>
      {showContactForm ? (
        <Modal
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
          open={showContactForm}
          onClose={() => setContactForm(!showContactForm)}
        >
          <Box alignItems="center">
            <ContactAddForm sx={{ color: "#fff" }} />
          </Box>
        </Modal>
      ) : null}
      <Navbar></Navbar>
      <Typography variant="h1" color="white">
        Contacts
      </Typography>
      {isContactsLoading ? (
        <FallingLines
          color="#fff"
          width="150"
          visible={true}
          ariaLabel="falling-lines-loading"
        />
      ) : null}

      {contacts && (
        <div>
          <Container maxWidth="lg">
            <TableContainer
              component={Paper}
              className={classes.tableContainer}
            >
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow className={classes.TableRow}>
                    <TableCell className={classes.tableHeaderCell}>
                      User Info
                    </TableCell>
                    <TableCell
                      className={classes.tableHeaderCell}
                      style={{ textAlign: "left" }}
                    >
                      Tasks
                    </TableCell>
                    <TableCell className={classes.tableHeaderCell}>
                      Actions
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {contacts?.map((contact) => (
                    <TableRow key={contact.id} className={classes.TableRow}>
                      <TableCell
                        align="center"
                        className={classes.tableBodyCell}
                      >
                        <Grid container spacing={2}>
                          <Grid item lg={2}>
                            <Avatar
                              alt={contact.first_name}
                              src="."
                              className={classes.avatar}
                            />
                          </Grid>
                          <Grid item lg={8}>
                            <Typography className={classes.name}>
                              #{contact.id} {contact.first_name}{" "}
                              {contact.last_name}
                            </Typography>
                            <Typography
                              color="#ffffff"
                              variant="body2"
                              textAlign="left"
                            >
                              {contact.nickname}
                            </Typography>
                          </Grid>
                        </Grid>
                      </TableCell>
                      <TableCell className={classes.tableBodyCell}>
                        {contact.tasksOfContact?.map((task) => (
                          <Box
                            display="flex"
                            sx={{ padding: 1 }}
                            flexDirection="column"
                          >
                            <Typography
                              className={classes.status}
                              sx={{ order: 1 }}
                              style={{
                                backgroundColor: "grey",
                                fontWeight: "bold",
                              }}
                            >
                              <Box
                                display="flex"
                                justifyContent="space-between"
                                flexWarp="no-wrap"
                                alignItems="center"
                                sx={{ padding: 0.4 }}
                              >
                                <Typography sx={{ order: 2 }}>
                                  <Link
                                    to={`/task/${task.task_id}`}
                                    style={{ color: "white" }}
                                  >
                                    #{task.task_id} {task.title}
                                  </Link>
                                </Typography>
                                <Typography
                                  sx={{ order: 3 }}
                                  className={classes.status}
                                  style={{
                                    fontWeight: "bold",
                                  }}
                                >
                                  {format(
                                    new Date(task.due),
                                    " HH : mm | dd/MM/yyyy"
                                  )}
                                </Typography>
                                <Typography
                                  sx={{ order: 3 }}
                                  className={classes.status}
                                  style={{
                                    backgroundColor:
                                      (task.label === "Canceled" && "red") ||
                                      (task.label === "Done" && "green") ||
                                      (task.label === "" && "orange"),
                                  }}
                                >
                                  {task.label === ""
                                    ? (task.label = "In Progress")
                                    : (task.label = task.label)}
                                </Typography>
                                <Assignment></Assignment>
                              </Box>
                            </Typography>
                          </Box>
                        ))}
                        <Box textAlign="center">
                          <Select
                            sx={{
                              boxShadow: "none",
                              ".MuiOutlinedInput-notchedOutline": {
                                border: 0,
                              },
                            }}
                            defaultValue=""
                            displayEmpty
                            renderValue={(value) => {
                              return (
                                <Box sx={{ display: "flex", gap: 1 }}>
                                  <SvgIcon
                                    style={{
                                      color: "grey",
                                      fontSize: 25,
                                    }}
                                  >
                                    <PostAdd />
                                  </SvgIcon>
                                  {value}
                                </Box>
                              );
                            }}
                          >
                            <div>
                              {tasks?.map((task) => (
                                <MenuItem
                                  key={task.id}
                                  onClick={() => {
                                    assignContactHandler(
                                      contact.id,
                                      task.task_id
                                    );
                                  }}
                                  disabled={task.disabled}
                                >
                                  {task.title}
                                </MenuItem>
                              ))}
                            </div>
                          </Select>
                        </Box>
                      </TableCell>

                      <TableCell className={classes.tableBodyCell}>
                        <IconButton>
                          <Edit></Edit>
                        </IconButton>
                        <IconButton
                          onClick={() => deleteContactHandler(contact.id)}
                        >
                          <Delete></Delete>
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Box>
              <IconButton
                style={{ color: "#b2ffb2" }}
                onClick={() => {
                  setContactForm(true);
                }}
              >
                <AddCircle sx={{ fontSize: 50 }} />
              </IconButton>
            </Box>
          </Container>
        </div>
      )}
    </div>
  );
};

export default ContactsPage;
