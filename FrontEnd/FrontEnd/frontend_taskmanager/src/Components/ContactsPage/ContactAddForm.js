import React, { useContext } from "react";

import {
  Box,
  TextField,
  Button,
  FormControl,
  Card,
  Typography,
  Alert,
  AlertTitle,
  Stack,
} from "../../../node_modules/@mui/material/index";

import { useMutation, useQueryClient } from "react-query";
import { postContact } from "../../Services/Api";
import { useState } from "react";
import { userContext } from "../../App";


const ContactAddForm = (props) => {
const user = useContext(userContext)
const [newContact, setNewContact] = useState();
const queryClient = new useQueryClient();
const { mutate, onMutate, status } = useMutation(postContact, {
  onSuccess: () => {
    queryClient.invalidateQueries("contacts");
  },
});

const addContactHandler = (e) => {
  setNewContact({
    ...newContact,
    contact_id: 30,
  });
  e.preventDefault();
  const contactParams = {contact:newContact, username:user.username};
  mutate(contactParams);
};
  return (
    <div>
      <div>
        {mutate.isSuccess && (
          <Alert
            severity="info"
            variant="filled"
            sx={{
              bgcolor: "#169016",
              marginTop: 4,
              marginBottom: 4,
              width: 500,
            }}
          >
            <AlertTitle>Task Added!</AlertTitle>
          </Alert>
        )}
        {mutate.isLoading && (
          <Alert
            severity="info"
            variant="filled"
            sx={{
              marginTop: 4,
              marginBottom: 4,
              width: 500,
            }}
          >
            <AlertTitle>loading...</AlertTitle>
          </Alert>
        )}

        <Box
          sx={{
            marginTop: 2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              //backgroundColor: "#FDF6FF",
              backgroundColor: "#444444",
              padding: 3,
              minHeight: 100,
              marginBottom: 10,
            }}
          >
            <FormControl>
              <Box component="form" noValidate sx={{ mt: 1, width: 500 }}>
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  First name
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  fullWidth
                  onChange={(e) =>
                    setNewContact({ ...newContact, first_name: e.target.value })
                  }
                  id="title"
                  name="email"
                  variant="filled"
                  autoFocus
                />
                  <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Last name
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  fullWidth
                  onChange={(e) =>
                    setNewContact({ ...newContact, last_name: e.target.value })
                  }
                  id="title"
                  name="email"
                  variant="filled"
                  autoFocus
                />
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Last name
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  fullWidth
                  onChange={(e) =>
                    setNewContact({ ...newContact, last_name: e.target.value })
                  }
                  id="title"
                  name="email"
                  variant="filled"
                  autoFocus
                />
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Nickname
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  variant="filled"
                  required
                  fullWidth
                  onChange={(e) =>
                    setNewContact({ ...newContact, nickname: e.target.value })
                  }
                  name="description"
                  rows={4}
                  label=""
                  id="description"
                />
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "green" }}
                  onClick={(e) => addContactHandler(e)}
                >
                  Add new contact
                </Button>
              </Box>
            </FormControl>
          </Card>
        </Box>
      </div>
    </div>
  );
};

export default ContactAddForm;
