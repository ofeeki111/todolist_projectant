import React, { useContext } from "react";
import { Chip, Stack, Typography } from "../../../node_modules/@mui/material/index";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { getTags } from "../../Services/Api";
import { userContext } from "../../App";
import Navbar from "../Navbar";

function TagsPage() {
  const queryClient = new useQueryClient();

  const user = useContext(userContext);
  const { data: tags, status: tagsStatus } = useQuery(
    "tags",
    () => getTags(user.session_id, user.username),
    {}
  );

  return (
    <div>
      <Navbar></Navbar>
      <Typography variant="h1" color="white">
        Tags
      </Typography>

      {tags?.map((tag)=>(
        <Chip
          label={tag.name}
          variant="filled"
          onDelete={() => {
          }}
        />
      ))
      }
       
    </div>
  );
}

export default TagsPage;
