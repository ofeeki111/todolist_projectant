import React from "react";
import {
  Card,
  Typography,
  Grid,
  Box,
  IconButton,
  Alert,
  Chip,
} from "@mui/material";
import { Link } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/Delete";
import { Edit } from "@mui/icons-material";
import { useMutation, useQueryClient } from "react-query";
import { deleteTask, postSubtask, editSubtask,editTask } from "../../Services/Api";
import { useState } from "react";
import { ThreeDots } from "react-loader-spinner";
import moment from "moment";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import DoneIcon from "@mui/icons-material/Done";

function SubtaskTaskPage(props) {
  const queryClient = new useQueryClient();

  const { mutate: DeleteSubtask, isLoading } = useMutation(deleteTask, {
    onSuccess: () => {queryClient.invalidateQueries("task")
    editTask(props.task)}
  });

  const { mutate: EditDoneSubtask } = useMutation(editSubtask, {
    onSuccess: () => {queryClient.invalidateQueries("task")
    editTask(props.task)}
  });
  const { mutate: EditCanceledSubtask } = useMutation(editSubtask, {
    onSuccess: () => {queryClient.invalidateQueries("task")
    editTask(props.task)}
  });

  const [subtaskObj, setSubtaskObj] = useState(props);

  const EditDoneSubtaskHandler = () => {
    if (subtaskObj.label === "Done") {
      console.log(subtaskObj);
      setSubtaskObj({ ...subtaskObj, label: "" });
      EditDoneSubtask({ ...subtaskObj, label: "" });
    } else {
      console.log(subtaskObj);
      setSubtaskObj({ ...subtaskObj, label: "Done" });
      EditDoneSubtask({ ...subtaskObj, label: "Done" });
    }
  };
  const EditCanceledSubtaskHandler = () => {
    if (subtaskObj.label === "Canceled") {
      setSubtaskObj({ ...subtaskObj, label: "" });
      EditCanceledSubtask({ ...subtaskObj, label: "" });
      console.log(subtaskObj);
    } else {
      setSubtaskObj({ ...subtaskObj, label: "Canceled" });
      EditCanceledSubtask({ ...subtaskObj, label: "Canceled" });
      console.log(subtaskObj);
    }
  };

  const deleteSubtaskHandler = () => {
    DeleteSubtask(props.task_id);
  };

  const d = new Date(props.due);

  return (
    <Grid item xs={12} justifyContent="center" >
      <Card
        className={subtaskObj.label == "Done" ? "done" : null}
        sx={{
          //backgroundColor: "#FDF6FF",

          backgroundColor: "#444444",
          padding: 3,
          width: 600,
          minHeight: 100,
        }}
      >
        {isLoading ? (
          <ThreeDots
            type="ThreeDots"
            color="#fff"
            width="100"
            height="100"
            wrapperStyle={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
          ></ThreeDots>
        ) : (
          <Box>
            <Grid item container direction="row" spacing={1}>
              <Grid item container xs={10} direction="row">
                <Grid item xs={6} spacing={1}>
                  <Typography
                    fontSize="2rem"
                    textAlign="left"
                    className={
                      subtaskObj.label == "Canceled" ? "canceled" : null
                    }
                    component={"span"}
                  >
                    <Typography fontSize="2rem" sx={{ color: "#FFFFFF" }}>
                      {props.title}
                    </Typography>

                    <Grid item xs={10}>
                      <Typography sx={{ color: "#b6b6b6 " }} fontSize={20}>
                        {props.description}
                      </Typography>
                      <Typography sx={{ color: "#b6b6b6 " }} fontSize={20}>
                        #{props.task_id} subtask of #{props.parentTask.task_id}
                      </Typography>
                      <Grid item xs={12}>
                        <Chip
                          label={moment(props.due).format(
                            " HH : MM | DD MMM YYYY"
                          )}
                          sx={{ color: "white" }}
                          variant="outlined"
                        />
                      </Grid>
                      <Grid item xs={12}>
                        {subtaskObj.label === "Canceled" && (
                          <Chip
                            label={subtaskObj.label}
                            sx={{ width: 100 }}
                            variant="outlined"
                            color="error"
                          />
                        )}
                        {subtaskObj.label === "Done" && (
                          <Chip
                            label={subtaskObj.label}
                            sx={{ color: "white", width: 100 }}
                            variant="filled"
                            color="success"
                          />
                        )}
                      </Grid>
                    </Grid>
                  </Typography>
                </Grid>
              </Grid>

              <Grid item container direction="column" align="right" xs={1}>
                <Grid item xs={12}>
                  <IconButton
                    aria-label="done"
                    size="large"
                    onClick={EditDoneSubtaskHandler}
                  >
                    <DoneIcon
                      style={{
                        backgroundColor:
                          subtaskObj.label == "Done" ? "#90ee90" : null,
                        color: "grey",
                      }}
                    />
                  </IconButton>
                  <IconButton
                    aria-label="cancel"
                    size="large"
                    onClick={EditCanceledSubtaskHandler}
                  >
                    <DoNotDisturbAltIcon
                      style={{
                        color:
                          subtaskObj.label == "Canceled" ? "f08080" : "grey",
                      }}
                    />
                  </IconButton>
                  <IconButton
                    aria-label="delete"
                    fontSize="inherit"
                    textAlign="right"
                    style={{ color: "grey" }}
                  >
                    <Edit fontSize="inherit" />
                  </IconButton>
                  <IconButton
                    aria-label="delete"
                    size="large"
                    onClick={deleteSubtaskHandler}
                  >
                    <DeleteIcon style={{ color: "grey" }} />
                  </IconButton>
                </Grid>
              </Grid>
            </Grid>
          </Box>
        )}
      </Card>
    </Grid>
  );
}

export default SubtaskTaskPage;
