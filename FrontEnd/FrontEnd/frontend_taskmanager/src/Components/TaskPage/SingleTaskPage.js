import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Grid, Card, Alert, AlertTitle, IconButton } from "@mui/material";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import { FallingLines } from "react-loader-spinner";
import { getTask } from "../../Services/Api";
import { useMutation, useQueryClient, useQuery } from "react-query";
import { postSubtask, editTask } from "../../Services/Api";
import Task from "../Task";
import SubtaskTaskPage from "./SubtaskTaskPage";
import TaskEditForm from "./TaskEditForm";
import {
  FormControlLabel,
  Switch,
  Typography,
} from "../../../node_modules/@mui/material/index";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import { Link } from "../../../node_modules/react-router-dom/index";
import { Add } from "../../../node_modules/@mui/icons-material/index";
import { orderTasksById } from "../../Services/SortService";
import { useHistory } from "react-router";
import "../css/TaskStyle.css";

function SingleTaskPage() {
  const [isEditMode, setIsEditMode] = useState(false);
  const { taskId } = useParams();
  const [task, setTask] = useState({});
  const { data, status } = useQuery(["task", taskId], getTask, {
    onSuccess: setTask,
  });

  const queryClient = new useQueryClient();

  const { mutate: addSubtask } = useMutation(postSubtask, {
    onSuccess: () => {
      editTask(task);
      queryClient.invalidateQueries("task");
      queryClient.invalidateQueries("tasks");
    },
  });

  const addSubtaskHandler = () => {
    const sub = {
      task_id: 30,
      title: "subtask",
      description: "1 task",
      due: new Date(),
      label: "",
      subTasks: [],
    };
    console.log(task);
    console.log(sub);
    const postArgs = { parent_task: data.task_id, subtask: sub };
    addSubtask(postArgs);
  };

  if (status === "error") {
    return <p>Error fetching data</p>;
  }
  if (status === "loading") {
    return (
      <FallingLines
        color="#fff"
        width="150"
        visible={true}
        ariaLabel="falling-lines-loading"
      />
    );
  }
  if (status === "success") {
    return (
      <div style={{ marginTop: 50, marginBottom: 100 }}>
        <Grid container direction="row" spacing={5} justifyContent="center">
          <Grid item container xs={10} spacing={2} align="center">
            <Grid item xs={12} align="center" spacing={2}>
              <Grid item xs={2} align="center">
                <Link
                  to={`/`}
                  className="link"
                  onClick={queryClient.invalidateQueries("tasks")}
                >
                  <IconButton aria-label="done">
                    <KeyboardReturnIcon
                      style={{
                        color: "grey",
                        fontSize: 40,
                      }}
                    ></KeyboardReturnIcon>
                  </IconButton>
                </Link>
              </Grid>

              <Switch
                checked={isEditMode}
                onChange={() => setIsEditMode(!isEditMode)}
                color="warning"
              />
              <Grid item xs={5} justifyContent="center">
                {isEditMode == true ? (
                  <TaskEditForm {...data}></TaskEditForm>
                ) : (
                  <Task {...data}></Task>
                )}
              </Grid>
              {!data.subTasks.length && (
                <Alert
                  severity="info"
                  variant="filled"
                  sx={{ bgcolor: "#444444", marginTop: 10, width: 500 }}
                >
                  <AlertTitle>No tasks!</AlertTitle>
                </Alert>
              )}

              {data?.subTasks?.length > 0 && (
                <Typography
                  textAlign="center"
                  fontSize="3rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Subtasks:
                </Typography>
              )}
              <IconButton style={{ color: "grey" }} onClick={addSubtaskHandler}>
                <Add sx={{ fontSize: 50 }} />
              </IconButton>
            </Grid>

            <Grid item container xs={12} align="center" spacing={2}>
              {data?.subTasks.map((subtask) => {
                return (
                  <SubtaskTaskPage
                    key={subtask.task_id}
                    {...subtask}
                    task={task}
                  ></SubtaskTaskPage>
                );
              })}
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default SingleTaskPage;
