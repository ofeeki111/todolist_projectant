import React from "react";
import {
  DateRange,
  DateRangeOutlined,
} from "../../../node_modules/@mui/icons-material/index";
import {
  Box,
  Avatar,
  TextField,
  Checkbox,
  Button,
  Grid,
  Link,
  FormControl,
  Card,
  Typography,
  Alert,
  AlertTitle,
  Switch,
  ToggleButton,
  Stack,
} from "../../../node_modules/@mui/material/index";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import moment from "../../../node_modules/moment/moment";
import { useMutation, useQueryClient } from "react-query";
import { deleteTask, editTask, postSubtask } from "../../Services/Api";
import { useState } from "react";

import "../css/TaskStyle.css";

function TaskEditForm(props) {
  const queryClient = new useQueryClient();

  const { status, mutate: EditTask } = useMutation(editTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("task");
    },
  });
  const [editTimeMode, setEditTimeMode] = useState(props.planned);
  const [newTask, setNewTask] = useState(props);

  const editTaskHandler = (e) => {
    e.preventDefault();
    EditTask(newTask);
  };

  return (
    <div>
      <div>
        {status == "success" && (
          <Alert
            severity="info"
            variant="filled"
            sx={{
              bgcolor: "#169016",
              marginTop: 4,
              marginBottom: 4,
              width: 500,
            }}
          >
            <AlertTitle>Task updated!</AlertTitle>
          </Alert>
        )}

        <Box
          sx={{
            marginTop: 2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              //backgroundColor: "#FDF6FF",
              backgroundColor: "#444444",
              padding: 3,
              minHeight: 100,
              marginBottom: 10,
            }}
          >
            <FormControl>
              <Typography
                textAlign="left"
                fontSize="3rem"
                sx={{ color: "#b8b8b8" }}
              >
                Edit Task #{props.task_id}
              </Typography>
              <Box component="form" noValidate sx={{ mt: 1 }}>
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Task Title
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  fullWidth
                  defaultValue={props.title}
                  onChange={(e) =>
                    setNewTask({ ...newTask, title: e.target.value })
                  }
                  id="title"
                  value={newTask.title}
                  name="email"
                  variant="filled"
                  autoFocus
                />
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Task description
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  variant="filled"
                  required
                  fullWidth
                  multiline
                  onChange={(e) =>
                    setNewTask({ ...newTask, description: e.target.value })
                  }
                  defaultValue={props.description}
                  value={newTask.description}
                  name="description"
                  rows={4}
                  label=""
                  id="description"
                />
                <Stack
                  direction="row"
                  alignItems="center"
                  justifyContent="space-between"
                >
                  <Typography
                    textAlign="left"
                    fontSize="1rem"
                    sx={{ color: "#FFFFFF" }}
                  >
                    Due to
                  </Typography>
                  <ToggleButton
                    checked={editTimeMode}
                    onChange={(e) => {
                      setEditTimeMode(!editTimeMode);
                      setNewTask({ ...newTask, planned: !e.target.checked});
                    }}
                    color="warning"
                    selected={editTimeMode}
                    value={editTimeMode}
                    exclusive
                  >
                    Planned for today
                  </ToggleButton>
                </Stack>
                {!editTimeMode ? (
                  <TextField
                    onChange={(e) =>
                      setNewTask({ ...newTask, due: e.target.value })
                    }
                    className="inputTextField"
                    fullWidth
                    margin="normal"
                    id="datetime"
                    type="datetime-local"
                    defaultValue={moment(props.due).format("YYYY-MM-DDThh:mm")}
                    sx={{ width: 500 }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                ) : (
                  <TextField
                    onChange={(e) =>
                    {
                      const date = new Date()
                      const hour = e.target.value.split(":")[0];
                      const min = e.target.value.split(":")[1];
                      date.setHours(hour)
                      date.setMinutes(min)
                      setNewTask({ ...newTask, due:date })
                    }
                      
                    }
                    className="inputTextField"
                    fullWidth
                    margin="normal"
                    id="datetime"
                    type="time"
                    defaultValue={moment(props.due).format("HH:MM")}
                    sx={{ width: 500 }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                )}

                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "green" }}
                  onClick={(e) => editTaskHandler(e)}
                >
                  Save Changes
                </Button>
              </Box>
            </FormControl>
          </Card>
        </Box>
      </div>
    </div>
  );
}

export default TaskEditForm;
