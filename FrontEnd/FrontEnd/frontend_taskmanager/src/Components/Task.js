import React, { useContext, useEffect } from "react";
import {
  Card,
  Typography,
  Grid,
  Box,
  IconButton,
  Alert,
  Chip,
} from "@mui/material";
import { Link } from "react-router-dom";
import DeleteIcon from "@mui/icons-material/Delete";
import { Edit } from "@mui/icons-material";
import { useMutation, useQueryClient, useQuery } from "react-query";
import Subtask from "./Subtask";
import "react-circular-progressbar/dist/styles.css";
import AddIcon from "@mui/icons-material/Add";
import {
  deleteTask,
  editTask,
  postSubtask,
  editSubtask,
  assignContact,
  removeContact,
  getContacts,
  getTags,
  addTagToTask,
  removeTagFromTask,
} from "../Services/Api";
import { useState } from "react";
import { ThreeDots } from "react-loader-spinner";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import DoneIcon from "@mui/icons-material/Done";
import Add from "@mui/icons-material/Add";
import "./css/TaskStyle.css";
import { isSubtasksDone, setSubtaskStatus } from "../Services/taskService";
import "animate.css";
import {
  CircularProgress,
  Divider,
  FormControl,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
  Stack,
  SvgIcon,
} from "../../node_modules/@mui/material/index";
import { format } from "date-fns";
import {
  AccountBox,
  BookmarkAdd,
  ContactPhone,
  PersonAdd,
  Tag,
} from "../../node_modules/@mui/icons-material/index";
import { userContext } from "../App";

function Task(props) {
  const user = useContext(userContext);
  const { mutate, isLoading } = useMutation(deleteTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });
  const { mutate: EditDone } = useMutation(editTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });
  const { mutate: EditCanceled } = useMutation(editTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const { mutate: addSubtask } = useMutation(postSubtask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const { mutate: addContact } = useMutation(assignContact, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const { mutate: deleteContact } = useMutation(removeContact, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const { mutate: addTag } = useMutation(addTagToTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const { mutate: deleteTag } = useMutation(removeTagFromTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const queryClient = new useQueryClient();
  const { data, status } = useQuery(
    ["contacts"],
    () => getContacts(user.session_id, user.username),
    {}
  );

  const { data: tagsData, status: tagsStatus } = useQuery(
    ["tags"],
    () => getTags(user.session_id, user.username),
    { onSuccess: () => {} }
  );

  const [contacts, setContacts] = useState([{}]);
  const [tags, setTags] = useState([{}]);

  useEffect(() => {
    if (status === "success") {
      const newContacts = data.map((contact) => {
        if (
          !props.contactsOfTask.some((tcontact) => tcontact.id === contact.id)
        )
          return { ...contact, disabled: false };
        else {
          return { ...contact, disabled: true };
        }
      });
      setContacts(newContacts);
    }
  }, [status, data]);

  useEffect(() => {
    if (status === "success") {
      const newTags = tags.map((tag) => {
        if (!props.tagsOfTask.some((ttag) => ttag.id === ttag.id))
          return { ...tag, disabled: false };
        else {
          return { ...tag, disabled: true };
        }
      });
      setTags(newTags);
    }
  }, [status, tagsData]);
  const [taskObj, setTaskObj] = useState({ ...props });

  const editDoneHandler = () => {
    if (isSubtasksDone(props)) {
      if (taskObj.label === "Done") {
        setTaskObj(prevTaskObj => ({ ...prevTaskObj, label: "" }));
        EditDone({ ...taskObj, label: "" });
      } else {
        setTaskObj(prevTaskObj => ({ ...prevTaskObj, label: "Done" }));
        EditDone({ ...taskObj, label: "Done" });
      }
    } else {
      props.setError({ isErr: true, content: "All tasks must be done before" });
    }
  };
  
  const editCanceledHandler = () => {
    if (taskObj.label === "Canceled") {
      setTaskObj(prevTaskObj => ({ ...prevTaskObj, label: "" }));
      EditCanceled({ ...taskObj, label: "" });
      setSubtaskStatus(taskObj, "");
      // edit task
    } else {
      setTaskObj(prevTaskObj => ({ ...prevTaskObj, label: "Canceled" }));
      EditCanceled({ ...taskObj, label: "Canceled" });
      setSubtaskStatus(taskObj, "Canceled");
    }
  };
  const deleteHandler = () => {
    if (props.subTasks.length) {
      props.setError({
        isErr: true,
        content: "Clear all subtasks before deleting",
      });
    } else {
      mutate(props.task_id);
    }
  };

  const addSubtaskHandler = () => {
    const sub = {
      task_id: 30,
      title: "subtask",
      description: "1 props",
      due: new Date().toJSON(),
      label: "",
      subTasks: [],
      planned: props.planned,
    };
    setTaskObj({ ...taskObj, label: "" });
    EditDone({ ...taskObj, label: "" });
    const postArgs = { parent_task: props.task_id, subtask: sub };

    addSubtask(postArgs);
  };

  const deleteContactHandler = (contact_id, task_id) => {
    const postArgs = { task_id: task_id, contact_id: contact_id };
    contacts?.map((contact) => {
      if (contact.id === contact_id) {
        let index = contacts.findIndex((contact) => contact.id === contact_id);
        contacts[index] = { ...contact, disabled: false };
        setContacts([...contacts]);
        setTaskObj({ ...taskObj, contactsOfTask: [...contacts] });
      }
    });
    deleteContact(postArgs);
  };

  const deleteTagHandler = (tag_id, task_id) => {
    const postArgs = { task_id: task_id, contact_id: tag_id };
    tagsData?.map((tags) => {
      if (tags.id === tag_id) {
        let index = contacts.findIndex((contact) => contact.id === tag_id);
        contacts[index] = { ...tags, disabled: false };
        // setTags([...contacts]);
        setTaskObj({ ...taskObj, tagsOfTask: [...tags] });
      }
    });
    deleteTag(postArgs);
  };

  const assignContactHandler = (contact_id, task_id) => {
    const postArgs = { task_id: task_id, contact_id: contact_id };
    contacts?.map((contact) => {
      if (contact.id === contact_id) {
        let index = contacts.findIndex((contact) => contact.id === contact_id);
        contacts[index] = { ...contact, disabled: true };
        setContacts([...contacts]);
        setTaskObj({ ...taskObj, contactsOfTask: [...contacts] });
      }
    });
    addContact(postArgs);
  };

  const addTagHandler = (tag_id, task_id) => {
    const postArgs = { task_id: task_id, tag_id: tag_id };
    tagsData?.map((tag) => {
      if (tag.id === tag) {
        let index = tagsData.findIndex((tag) => tag.id === tag_id);
        tagsData[index] = { ...tags, disabled: true };
        // setTags([...tags]);
        setTaskObj({ ...taskObj, tagsOfTask: [...tags] });
      }
    });
    addTag(postArgs);
  };

  const d = new Date(props.due);
  return (
    <Card
      className={taskObj.label == "Done" ? "done" : null}
      sx={{
        //backgroundColor: "#FDF6FF",
        backgroundColor: props.planned ? "#383838" : "#444444",
        padding: 3,
        maxWidth: "30rem",
        marginTop: 3,
      }}
    >
      {isLoading ? (
        <ThreeDots
          type="ThreeDots"
          color="#fff"
          width="100"
          height="100"
          wrapperStyle={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
          }}
        ></ThreeDots>
      ) : (
        <Grid item container direction="row" spacing={1}>
          <Grid item container xs={10} direction="row">
            <Grid item xs={12} spacing={1}>
              <Typography
                fontSize="2rem"
                textAlign="left"
                className={taskObj.label == "Canceled" ? "canceled" : null}
                component={"span"}
              >
                <Link to={`/task/${props.task_id}`} className="link">
                  <Typography fontSize="2rem" sx={{ color: "#FFFFFF" }}>
                    {props.title}
                  </Typography>
                </Link>
                <Grid item xs={10}>
                  <Typography sx={{ color: "#b6b6b6" }} fontSize={20}>
                    {props.description}
                  </Typography>
                  <Typography sx={{ color: "#b6b6b6" }} fontSize={20}>
                    #{props.task_id}
                  </Typography>
                </Grid>
              </Typography>
              <Grid
                item
                container
                direction="row"
                xs={8}
                align="left"
                spacing={1}
                sx={{ marginTop: 2 }}
              >
                <Grid item xs={12}>
                  <Chip
                    label={format(d, " HH : mm | dd/MM/yyyy")}
                    sx={{ color: "white" }}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  {taskObj.label === "Canceled" && (
                    <Chip
                      sx={{ marginTop: 1,marginBottom:2 }}
                      label={taskObj.label}
                      variant="outlined"
                      color="error"
                    />
                  )}
                  {taskObj.label === "Done" && (
                    <Chip
                      label={taskObj.label}
                      sx={{ color: "white", width: 100, margin: 1 }}
                      variant="filled"
                      color="success"
                    />
                  )}
                </Grid>
              </Grid>
            </Grid>
            <Stack direction="column">
              {props.contactsOfTask ? (
                <Stack
                  direction="row"
                  alignItems="center"
                  spacing={5}
                  flexWrap="wrap"
                >
                  {props.contactsOfTask.map((contact) => (
                    <div
                      key={contact.id}
                      className="ContactOfTask"
                      onClick={() => {
                        deleteContactHandler(contact.id, props.task_id);
                      }}
                    >
                      <Stack direction="row" alignItems="center" spacing={1} sx={{marginBottom:2}}>
                        <AccountBox style={{ color: "#b6b6b6" }}></AccountBox>
                        <h5 style={{ color: "#b6b6b6" }}>
                          {contact.first_name}
                        </h5>
                      </Stack>
                    </div>
                  ))}
                </Stack>
              ) : null}
              {props.tagsOfTask ? (
                <Stack
                  direction="row"
                  alignItems="center"
                  spacing={2}
                  flexWrap="wrap"
                >
                  {props.tagsOfTask.map((tag) => (
                    <div
                      key={tag.id}
                      onClick={() => {
                        deleteContactHandler(tag.id, props.task_id);
                      }}
                    >
                      <Stack direction="row" alignItems="center" spacing={1}>
                        <Chip
                          style={{backgroundColor:tag.colorOfTag}}
                          label={tag.name}
                          variant="filled"
                          onDelete={() => {
                            deleteTagHandler(tag.id, props.task_id);
                          }}
                        />
                      </Stack>
                    </div>
                  ))}

                  <Select
                    sx={{
                      boxShadow: "none",
                      ".MuiOutlinedInput-notchedOutline": { border: 0 },
                    }}
                    defaultValue=""
                    displayEmpty
                    renderValue={(value) => {
                      return (
                        <Box sx={{ display: "flex", gap: 1 }}>
                          <SvgIcon style={{ color: "grey", fontSize: 30 }}>
                            <Tag />
                          </SvgIcon>
                          {value}
                        </Box>
                      );
                    }}
                  >
                    <div>
                      {tagsData?.map((tag) => (
                        <MenuItem
                          key={tag.id}
                          onClick={() => {
                            addTagHandler(tag.id, props.task_id);
                          }}
                          disabled={tag.disabled}
                        >
                          {tag.name}
                        </MenuItem>
                      ))}
                    </div>
                  </Select>
                </Stack>
              ) : null}
            </Stack>
            <Grid item xs={12}>
              {props.subTasks?.map((sub) => {
                return (
                  <Subtask
                    key={sub.task_id}
                    parent_id={props.task_id}
                    {...sub}
                    task={props}
                  ></Subtask>
                );
              })}
            </Grid>
          </Grid>

          <Grid item xs={2}>
            <Stack
              divider={
                <Divider orientation="horizontal" sx={{ bgcolor: "#b6b6b6" }} />
              }
            >
              <IconButton
                aria-label="done"
                size="large"
                onClick={editDoneHandler}
              >
                <DoneIcon
                  style={{
                    backgroundColor: taskObj.label == "Done" ? "#90ee90" : null,
                    color: "grey",
                  }}
                />
              </IconButton>
              <IconButton
                aria-label="cancel"
                size="large"
                onClick={editCanceledHandler}
              >
                <DoNotDisturbAltIcon
                  style={{
                    color: taskObj.label == "Canceled" ? "f08080" : "grey",
                  }}
                />
              </IconButton>{" "}
              <Select
                sx={{
                  boxShadow: "none",
                  ".MuiOutlinedInput-notchedOutline": { border: 0 },
                }}
                defaultValue=""
                displayEmpty
                renderValue={(value) => {
                  return (
                    <Box sx={{ display: "flex", gap: 1 }}>
                      <SvgIcon style={{ color: "grey", fontSize: 30 }}>
                        <PersonAdd />
                      </SvgIcon>
                      {value}
                    </Box>
                  );
                }}
              >
                <div>
                  {contacts?.map((contact) => (
                    <MenuItem
                      key={contact.id}
                      onClick={() => {
                        assignContactHandler(contact.id, props.task_id);
                      }}
                      disabled={contact.disabled}
                    >
                      {contact.first_name}
                    </MenuItem>
                  ))}
                </div>
              </Select>
              <IconButton
                aria-label="delete"
                size="large"
                onClick={deleteHandler}
              >
                <DeleteIcon style={{ color: "grey" }} />
              </IconButton>
              <IconButton style={{ color: "grey" }} onClick={addSubtaskHandler}>
                {" "}
                <Add sx={{ fontSize: 40 }} />
              </IconButton>
            </Stack>
          </Grid>
        </Grid>
      )}
    </Card>
  );
}

export default Task;
