import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { LockOutlined } from "@mui/icons-material";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Navbar from "../Navbar";
import { useMutation, useQueryClient } from "react-query";
import { signInUser, signUpUser } from "../../Services/Api";
import { Alert, AlertTitle } from "../../../node_modules/@mui/material/index";
import { useEffect } from "react";
import { FallingLines } from "../../../node_modules/react-loader-spinner/dist/index";
import { Redirect } from "react-router";
import { Navigate, useNavigate } from "../../../node_modules/react-router-dom/index";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "50vh",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundColor: theme.palette.type === "dark",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#fffff",
  },
  size: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2b2b2b",
  },

  paper: {
    margin: theme.spacing(2, 6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  TextField: {
    backgroundColor: "#757171",
  },
  avatar: {
    margin: theme.spacing(0),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    alignItems: "center",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const RegisterPage = (props) => {
  const navigate = useNavigate()
  const queryClient = new useQueryClient();
  const [user, setUser] = useState({ username: "", password: "" });
  const [error, setError] = useState({ isError: false, msg: "" });
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    document.title = "Register";
  });

  useEffect(() => {
    const timeId = setTimeout(() => {
      // After 3 seconds set the show value to false
      setError({ ...error, isError: false, msg: "" });
    }, 8000);

    return () => {
      clearTimeout(timeId);
    };
  });

  const {
    mutate: userSignUpMutation,
    onMutate,
    status: registerStatus,
  } = useMutation(signUpUser, {
    onSuccess: (userResponse) => {
      setError({ isError: false });
      localStorage.setItem('currentUser',  JSON.stringify(userResponse))
      setSuccess(true);
      localStorage.setItem("currentUser", JSON.stringify(userResponse));
      navigate("/")   
      navigate(0)   
     },
    onError: (error) => {
      setError({ isError: true, msg: "User already exists" });
    },
  });
  const signUserHandler = (e) => {
    e.preventDefault();
    userSignUpMutation(user);

  };
  const classes = useStyles();
  return (
    <div>
      <Grid container direction="row" spacing={2} justifyContent="center">
        {error.isError ? (
          <Alert
            variant="standard"
            severity="error"
            className="animate__shakeX"
            color="error"
          >
            {error.msg}{" "}
          </Alert>
        ) : (
          <></>
        )}
      </Grid>

      <Grid container component="main" className={classes.root}>
        <Grid
          className={classes.size}
          item
          xs={12}
          sm={10}
          md={8}
          xl={3}
          component={Paper}
          elevation={1}
          square
        >
          {registerStatus === "loading" ? (
            <FallingLines
              color="#fff"
              width="150"
              visible={true}
              ariaLabel="falling-lines-loading"
            />
          ) : (
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlined />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign up
              </Typography>
              <form
                className={classes.form}
                noValidate
                onSubmit={(e) => signUserHandler(e)}
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoFocus
                  onChange={(e) =>
                    setUser({ ...user, username: e.target.value })
                  }
                  className={classes.TextField}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={(e) =>
                    setUser({ ...user, password: e.target.value })
                  }
                  className={classes.TextField}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <Button
                  fullWidth
                  variant="contained"
                  type="submit"
                  color="primary"
                  className={classes.submit}
                >
                  Sign Up
                </Button>
                <Grid container>
                  <Grid item>
                    <Link href="/" variant="body2">
                      {"Have an account? Sign In"}
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          )}
        </Grid>
      </Grid>
    </div>
  );
};

export default RegisterPage;
