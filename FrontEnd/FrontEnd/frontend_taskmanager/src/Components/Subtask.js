import {
  Box,
  CardContent,
  Grid,
  Paper,
  Typography,
  IconButton,
} from "@mui/material";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import DoneIcon from "@mui/icons-material/Done";
import React from "react";
import CloseIcon from "@mui/icons-material/Close";
import { useMutation, useQueryClient } from "react-query";
import { deleteTask } from "../Services/Api";

function Subtask(props) {
  const queryClient = new useQueryClient();
  const { mutate, isLoading } = useMutation(deleteTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");

    },
  });

  const deleteSubtaskHandler = () => {
    mutate(props.task_id);
  };
  return (
    <div>
      <Paper
        sx={{
          backgroundColor: "#6a6a6a",
        }}
      >
        <Grid
          container
          direction="row"
          spacing={0}
          alignItems="center"
          justifyContent="center"
          sx={{
            padding: 0.2,
            margin: 1,
          }}
        >
          <Grid item container xs={8}>
            <Grid item xs={10} align="left">
              <Typography textAlign="left" color="grey" item xs={6}>
                {props.title}
              </Typography>
            </Grid>
         
                <Grid item xs={2} align="left">
                  {props.label === "Done" && (
                    <DoneIcon style={{ color: "#90EE90", fontSize: 25 }} />
                  )}
                  {props.label === "Canceled" && (
                    <DoNotDisturbAltIcon
                      style={{ color: "#ff4d4d", fontSize: 20 }}
                    />
                  )}
                </Grid>
              
          </Grid>
          <Grid item xs={2} align="right">
            <IconButton
              aria-label="delete"
              size="large"
              onClick={deleteSubtaskHandler}
            >
              <CloseIcon fontSize="inherit" style={{ color: "white" }} />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
}

export default Subtask;
