import React, { useContext } from "react";
import {
  DateRange,
  DateRangeOutlined,
} from "../../node_modules/@mui/icons-material/index";
import {
  Box,
  Avatar,
  TextField,
  Checkbox,
  Button,
  Grid,
  Link,
  FormControl,
  Card,
  Typography,
  Alert,
  AlertTitle,
  Switch,
  ToggleButton,
  Stack,
} from "../../node_modules/@mui/material/index";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import moment from "../../node_modules/moment/moment";
import { useMutation, useQueryClient } from "react-query";
import { deleteTask, editTask, postTask } from "../Services/Api";
import { useState } from "react";

import "./css/TaskStyle.css";
import { formatTime } from "../Services/taskService";
import { format } from "date-fns/esm";
import { userContext } from "../App";

function TaskAddForm(props) {
  const user = useContext(userContext)
  const [newTask, setNewTask] = useState();
  const queryClient = new useQueryClient();
  const { mutate, onMutate, status } = useMutation(postTask, {
    onSuccess: () => {
      queryClient.invalidateQueries("tasks");
    },
  });

  const addTaskHandler = (e) => {
    e.preventDefault()
    setNewTask({
      ...newTask,
      task_id: 30,
      label: "",
      planned: props.isPlanned,
    });
    const params = { task: newTask, username: user.username };
    e.preventDefault();
    mutate(params);
  };

  return (
    <div>
      <div>
        {mutate.isSuccess && (
          <Alert
            severity="info"
            variant="filled"
            sx={{
              bgcolor: "#169016",
              marginTop: 4,
              marginBottom: 4,
              width: 500,
            }}
          >
            <AlertTitle>Task Added!</AlertTitle>
          </Alert>
        )}
        {mutate.isLoading && (
          <Alert
            severity="info"
            variant="filled"
            sx={{
              marginTop: 4,
              marginBottom: 4,
              width: 500,
            }}
          >
            <AlertTitle>loading...</AlertTitle>
          </Alert>
        )}

        <Box
          sx={{
            marginTop: 2,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Card
            sx={{
              //backgroundColor: "#FDF6FF",
              backgroundColor: "#444444",
              padding: 3,
              minHeight: 100,
              marginBottom: 10,
            }}
          >
            <FormControl>
              <Box component="form" noValidate sx={{ mt: 1, width: 500 }}>
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Task Title
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  fullWidth
                  onChange={(e) =>
                    setNewTask({ ...newTask, title: e.target.value })
                  }
                  id="title"
                  name="email"
                  variant="filled"
                  autoFocus
                />
                <Typography
                  textAlign="left"
                  fontSize="1rem"
                  sx={{ color: "#FFFFFF" }}
                >
                  Task description
                </Typography>
                <TextField
                  className="inputTextField"
                  margin="normal"
                  variant="filled"
                  required
                  fullWidth
                  multiline
                  onChange={(e) =>
                    setNewTask({ ...newTask, description: e.target.value })
                  }
                  name="description"
                  rows={4}
                  label=""
                  id="description"
                />
                <Stack
                  direction="row"
                  alignItems="center"
                  justifyContent="space-between"
                >
                  <Typography
                    textAlign="left"
                    fontSize="1rem"
                    sx={{ color: "#FFFFFF" }}
                  >
                    Due to
                  </Typography>
                </Stack>

                <TextField
                  onChange={
                    props.isPlanned
                      ? (e) => {
                          setNewTask({
                            ...newTask,
                            planned: props.isPlanned,
                            due: format(
                              new Date(),
                              "yyyy-MM-dd'T" + e.target.value
                            ),
                          });
                        }
                      : (e) =>
                          setNewTask({
                            ...newTask,
                            planned: props.isPlanned,
                            due: e.target.value,
                          })
                  }
                  className="inputTextField"
                  fullWidth
                  margin="normal"
                  id="datetime"
                  type={props.isPlanned ? "time" : "datetime-local"}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />

                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "green" }}
                  onClick={(e) => addTaskHandler(e)}
                >
                  Add new task
                </Button>
              </Box>
            </FormControl>
          </Card>
        </Box>
      </div>
    </div>
  );
}

export default TaskAddForm;
