import React, { useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { LockOutlined } from "@mui/icons-material";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Navbar from "../Navbar";
import { useMutation, useQueryClient } from "react-query";
import { signInUser } from "../../Services/Api";
import { Alert } from "../../../node_modules/@mui/material/index";
import { useEffect } from "react";
import {
  FallingLines,
  ThreeDots,
} from "../../../node_modules/react-loader-spinner/dist/index";
import { Redirect } from "react-router";
import { Navigate, useNavigate } from "../../../node_modules/react-router-dom/index";
import Stack from "../../../node_modules/lodash/_Stack";

const useStyles = makeStyles((theme) => ({
  root: {
    height: "50vh",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundColor: theme.palette.type === "dark",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "#fffff",
  },
  size: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2b2b2b",
  },

  paper: {
    margin: theme.spacing(2, 6),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  TextField: {
    backgroundColor: "#757171",
  },
  avatar: {
    margin: theme.spacing(0),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%",
    alignItems: "center",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const LoginPage = (props) => {
  const queryClient = new useQueryClient();
  const navigate = useNavigate()
  const [user, setUser] = useState({ username: "", password: "" });
  const [error, setError] = useState({ isError: false, msg: "" });
  const [success, setSuccess] = useState(false);

  useEffect(() => {
    document.title = "Login";
  });

  useEffect(() => {
    const timeId = setTimeout(() => {
      // After 3 seconds set the show value to false
      setError({ ...error, isError: false, msg: "" });
    }, 8000);

    return () => {
      clearTimeout(timeId);
    };
  });

  const {
    mutate: userSignInMutation,
    onMutate,
    status: loginStatus,
  } = useMutation(signInUser, {
    onSuccess: (userResponse) => {
      setError({ isError: false });
      setSuccess(true);
      localStorage.setItem("currentUser", JSON.stringify(userResponse));
      navigate(0)
    },
    onError: (error) => {
      setError({ isError: true, msg: "User or password are not correct!" });
    },
  });
  const signUserHandler = (e) => {
    e.preventDefault();
    userSignInMutation(user);
  };
  const classes = useStyles();
  return (
    <div>
      <Grid container direction="column" spacing={2} justifyContent="center">
        {error.isError ? (
          <Alert
            variant="standard"
            severity="error"
            className="animate__shakeX"
            color="error"
          >
            {error.msg}{" "}
          </Alert>
        ) : (
          <></>
        )}
      </Grid>
      {success && (
        <Alert variant="filled" severity="success">
          Signed in successfully! Redirecting
        </Alert>
      )}
      <Grid container component="main" className={classes.root}>
        <Grid
          className={classes.size}
          item
          xs={12}
          sm={10}
          md={8}
          xl={3}
          component={Paper}
          elevation={1}
          square
        >
          {loginStatus === "loading" ? (
            <FallingLines
              color="#fff"
              width="150"
              visible={true}
              ariaLabel="falling-lines-loading"
            />
          ) : (
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlined />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form
                className={classes.form}
                noValidate
                onSubmit={(e) => signUserHandler(e)}
              >
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoFocus
                  onChange={(e) =>
                    setUser({ ...user, username: e.target.value })
                  }
                  className={classes.TextField}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={(e) =>
                    setUser({ ...user, password: e.target.value })
                  }
                  className={classes.TextField}
                />
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />

                <Button
                  fullWidth
                  variant="contained"
                  type="submit"
                  color="primary"
                  className={classes.submit}
                >
                  Sign In
                </Button>
                <Grid container>
                  <Grid item>
                    <Link href="/register" variant="body2">
                      {"Don't have an account? Sign Up"}
                    </Link>
                  </Grid>
                </Grid>
              </form>
            </div>
          )}
          {success && <ThreeDots></ThreeDots>}
        </Grid>
      </Grid>
    </div>
  );
};

export default LoginPage;
