export const orderTasksById = (data) => {
    let sorted_tasks = data?.sort((a, b) => {
        return a.task_id - b.task_id;
      })
      .reverse();
    return sorted_tasks;
  };