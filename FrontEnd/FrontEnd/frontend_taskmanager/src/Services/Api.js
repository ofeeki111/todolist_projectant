import { format, parseISO } from "date-fns";
import { useContext } from "react";
import { useQuery, useMutation, useQueryClient } from "react-query";
import moment from "../../node_modules/moment/moment";

Date.prototype.toJSON = function () {
  return moment(this).format("yyyy-MM-DDTHH:mm:ss");
};
export const deleteTask = async (task_id) => {
  const res = await fetch("http://localhost:8080/api/tasks/delete/" + task_id, {
    method: "DELETE",
  });
};

export const deleteContact = async (contact_id) => {
  const res = await fetch(
    "http://localhost:8080/api/contacts/" + contact_id + "/delete",
    {
      method: "DELETE",
    }
  );
};

export const deleteTag = async (params) => {
  const res = await fetch(
    `http://localhost:8080/api/users/${params.username}/tags/${params.tag_id}/delete`,
    {
      method: "DELETE",
    }
  );
};

export const editTask = async (task) => {
  try {
    const res = await fetch("http://localhost:8080/api/tasks/update/", {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(
       task
 ),
    });
    console.log( JSON.stringify(
      task
))
    if (!res.ok) {
      throw new Error("Failed to update task" + res.error);
    
    }
    return await res.json();
  } catch (error) {
    console.error("Error updating task:");
    throw error;
  }
};


export const editSubtask = async (subtask) => {
  const res = await fetch(`http://localhost:8080/api/tasks/subtasks/edit`, {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      ...subtask,
    }),
  });
};

export const postTask = async (params) => {
  var date = new Date();
  date = JSON.stringify(date);
  const res = await fetch(
    `http://localhost:8080/api/users/${params.username}/tasks/add/`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        task_id: 40,
        label: "",
        due: params.due,
        ...params.task,
      }),
    }
  );
};

export const postContact = async (params) => {
  const res = await fetch(
    `http://localhost:8080/api/users/${params.username}/contacts/add/`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        ...params.contact,
      }),
    }
  );
};

export const postTag = async (params) => {
  const res = await fetch(
    `http://localhost:8080/api/users/${params.username}/tags/add/`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        ...params.tag,
      }),
    }
  );
};

export const signInUser = async (user) => {
  const res = await fetch("http://localhost:8080/api/users/login", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify({
      ...user,
    }),
  });
  if (res.ok) {
    const resjson = await res.json();
    console.log(resjson);
    return resjson;
  } else if (res.status == 500) {
    throw new Error("user not found");
  }
};

export const signUpUser = async (user) => {
  const res = await fetch("http://localhost:8080/api/users/register", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify({
      ...user,
    }),
  });
  if (res.ok) {
    const resjson = await res.json();
    console.log(resjson);
    return resjson;
  } else if (res.status == 500) {
    throw new Error("user not found");
  }
};

export const postSubtask = async (subtaskParams) => {
  const res = await fetch(
    `http://localhost:8080/api/tasks/${subtaskParams.parent_task}/subtasks/add/`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(subtaskParams.subtask),
    }
  );
};

export const assignContact = async (contactParams) => {
  const res = await fetch(
    `http://localhost:8080/api/tasks/${contactParams.task_id}/assign/${contactParams.contact_id}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
};

export const addTagToTask = async (contactParams) => {
  const res = await fetch(
    `http://localhost:8080/api/tasks/${contactParams.task_id}/tags/add/${contactParams.tag_id}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
};

export const removeContact = async (contactParams) => {
  const res = await fetch(
    `http://localhost:8080/api/tasks/${contactParams.task_id}/remove/${contactParams.contact_id}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
};

export const removeTagFromTask = async (contactParams) => {
  const res = await fetch(
    `http://localhost:8080/api/tasks/${contactParams.task_id}/remove/${contactParams.tag_id}`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }
  );
};

export const getTask = async ({ queryKey }) => {
  const [_, task_id] = queryKey;
  const res = await fetch(`http://localhost:8080/api/tasks/${task_id}`);
  const resjson = await res.json();
  return resjson;
};
export const getContacts = async (session_id, username) => {
  const res = await fetch(
    `http://localhost:8080/api/users/${username}/contacts/`,
    {
      method: "GET",
      headers: {
        "Content-Type": "text/plain",
        session_id: session_id,
      },
    }
  );
  return res.json();
};

export const getTags = async (session_id, username) => {
  const res = await fetch(
    `http://localhost:8080/api/users/${username}/tags/`,
    {
      method: "GET",
      headers: {
        "Content-Type": "text/plain",
        session_id: session_id,
      },
    }
  );
  return res.json();
};


export const fetchTasksHandler = async (session_id, username) => {
  const res = await fetch(`http://localhost:8080/api/users/${username}/tasks`, {
    method: "GET",
    headers: {
      "Content-Type": "text/plain",
      session_id: session_id,
    },
  });
  return res.json();
};
