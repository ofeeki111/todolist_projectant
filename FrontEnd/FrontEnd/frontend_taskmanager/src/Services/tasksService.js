import moment from "moment";
import { orderTasksById } from "./SortService";


export const getAllTasksForToday = (date, tasks, planned, showAll) => {
  let tasksForToday = [];

  tasks.forEach((task) => {
    let formattedDue = moment(task.due).format('MM/DD/YYYY');
    const formattedDate = moment(date).format('MM/DD/YYYY');
    if (showAll && task.planned == planned ){
      tasksForToday.push(task);
    }
    else if ( task.planned == planned && formattedDate == formattedDue) {
      tasksForToday.push(task);
    }
  });
  return orderTasksById(tasksForToday);
};


