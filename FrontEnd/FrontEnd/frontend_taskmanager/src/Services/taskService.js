import { format, parseISO } from "date-fns";
import { editSubtask } from "../Services/Api";

export const isSubtasksDone = (task) => {
  let isSubtaskDone = true;
  task.subTasks.forEach((subtask) => {
    if (subtask.label != "Done") {
      isSubtaskDone = false;
      return false;
    }
  });
  return isSubtaskDone;
};

export const setSubtaskStatus = (task, status) => {
  console.log(status);
  task.subTasks.forEach((subtask) => {
    if (subtask.label != status) {
      subtask.label = status;
    }
    editSubtask(subtask);
  });
  console.log("task");
};

// export const getDonePercentage = (task) => {
//   console.log("called")
//   let numOfDone = 0;
//   task?.subTasks?.forEach((subtask) => {
//     if (subtask.label == "Done") {
//       numOfDone++;
//     }
//   });
//   let len = task.subTasks?.length > 0 ? task.subTasks.length : 0;
//   console.log(len)
//   console.log(numOfDone)
//   let donePercentage = len > 0 ? (100 / len) * numOfDone : 0;
//   return donePercentage;
// };

export const formatTime = (dateString) => {
  console.log(dateString);
  const date = format(new Date(), "yyyy-MM-dd'T'" + dateString);
  console.log(date);
  return format(new Date(parseISO(date)), "yyyy-MM-dd'T'hh:mm");
};
