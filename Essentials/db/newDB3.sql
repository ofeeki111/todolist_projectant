--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.4

-- Started on 2022-10-18 22:50:05

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 217 (class 1259 OID 21520)
-- Name: contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact (
    id integer NOT NULL,
    first_name text,
    last_name text,
    nickname text,
    userofcontacts_id integer,
    userofcontact_id bigint
);


ALTER TABLE public.contact OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 21519)
-- Name: contact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contact_id_seq OWNER TO postgres;

--
-- TOC entry 3459 (class 0 OID 0)
-- Dependencies: 216
-- Name: contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contact_id_seq OWNED BY public.contact.id;


--
-- TOC entry 218 (class 1259 OID 21528)
-- Name: contact_in_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_in_group (
    contact_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.contact_in_group OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 38625)
-- Name: contact_tasksofcontact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contact_tasksofcontact (
    contactsoftask_id integer NOT NULL,
    tasksofcontact_task_id integer NOT NULL,
    contact_id integer NOT NULL
);


ALTER TABLE public.contact_tasksofcontact OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16558)
-- Name: group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."group" (
    id integer NOT NULL,
    name text
);


ALTER TABLE public."group" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16557)
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO postgres;

--
-- TOC entry 3460 (class 0 OID 0)
-- Dependencies: 209
-- Name: group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.group_id_seq OWNED BY public."group".id;


--
-- TOC entry 215 (class 1259 OID 21511)
-- Name: group_of_contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_of_contact (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.group_of_contact OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 21510)
-- Name: group_of_contact_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.group_of_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_of_contact_id_seq OWNER TO postgres;

--
-- TOC entry 3461 (class 0 OID 0)
-- Dependencies: 214
-- Name: group_of_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.group_of_contact_id_seq OWNED BY public.group_of_contact.id;


--
-- TOC entry 230 (class 1259 OID 30021)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 38620)
-- Name: hibernate_sequences; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.hibernate_sequences (
    sequence_name character varying(255) NOT NULL,
    next_val bigint
);


ALTER TABLE public.hibernate_sequences OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 21545)
-- Name: mail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mail (
    email_address text NOT NULL,
    contact_id integer NOT NULL
);


ALTER TABLE public.mail OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 21538)
-- Name: phone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.phone (
    phone_number text NOT NULL,
    contact_id integer NOT NULL
);


ALTER TABLE public.phone OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 21533)
-- Name: subgroups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subgroups (
    group_id integer NOT NULL,
    subgroup_id integer NOT NULL
);


ALTER TABLE public.subgroups OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 21553)
-- Name: tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tags (
    id integer NOT NULL,
    title text
);


ALTER TABLE public.tags OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 21552)
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tags_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tags_id_seq OWNER TO postgres;

--
-- TOC entry 3462 (class 0 OID 0)
-- Dependencies: 222
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tags_id_seq OWNED BY public.tags.id;


--
-- TOC entry 224 (class 1259 OID 21561)
-- Name: tags_in_tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tags_in_tasks (
    tags_id integer NOT NULL,
    tasks_id integer NOT NULL
);


ALTER TABLE public.tags_in_tasks OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 21502)
-- Name: task; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task (
    task_id integer NOT NULL,
    parent_id integer,
    blocked_by integer,
    blocking integer,
    title text NOT NULL,
    description text NOT NULL,
    due timestamp with time zone,
    label text NOT NULL,
    parenttask_task_id integer,
    is_planned boolean DEFAULT false NOT NULL,
    planned boolean,
    useroftasks_id integer,
    useroftask_id bigint
);


ALTER TABLE public.task OWNER TO postgres;

--
-- TOC entry 226 (class 1259 OID 21653)
-- Name: task_blocking; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task_blocking (
    task_task_id integer NOT NULL,
    blocking_task_id integer NOT NULL
);


ALTER TABLE public.task_blocking OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 29994)
-- Name: task_contact; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task_contact (
    contact_id integer,
    task_id integer
);


ALTER TABLE public.task_contact OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 38675)
-- Name: task_contactsoftask; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task_contactsoftask (
    tasksofcontact_task_id integer NOT NULL,
    contactsoftask_id integer NOT NULL
);


ALTER TABLE public.task_contactsoftask OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 21481)
-- Name: task_sub_tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.task_sub_tasks (
    task_task_id integer NOT NULL,
    sub_tasks_task_id integer NOT NULL
);


ALTER TABLE public.task_sub_tasks OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 21501)
-- Name: task_task_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.task_task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.task_task_id_seq OWNER TO postgres;

--
-- TOC entry 3463 (class 0 OID 0)
-- Dependencies: 212
-- Name: task_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.task_task_id_seq OWNED BY public.task.task_id;


--
-- TOC entry 228 (class 1259 OID 30007)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    password character varying(255),
    username character varying(255),
    session_id character varying(255)
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 30014)
-- Name: user_contacts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_contacts (
    appusr_id integer NOT NULL,
    contacts_id integer NOT NULL
);


ALTER TABLE public.user_contacts OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 30414)
-- Name: user_tasksofuser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_tasksofuser (
    appusr_id bigint NOT NULL,
    tasksofuser_task_id integer NOT NULL
);


ALTER TABLE public.user_tasksofuser OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 21630)
-- Name: userrel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.userrel (
    userid integer NOT NULL,
    parentid integer NOT NULL
);


ALTER TABLE public.userrel OWNER TO postgres;

--
-- TOC entry 3249 (class 2604 OID 21523)
-- Name: contact id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact ALTER COLUMN id SET DEFAULT nextval('public.contact_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 16561)
-- Name: group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."group" ALTER COLUMN id SET DEFAULT nextval('public.group_id_seq'::regclass);


--
-- TOC entry 3248 (class 2604 OID 21514)
-- Name: group_of_contact id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_of_contact ALTER COLUMN id SET DEFAULT nextval('public.group_of_contact_id_seq'::regclass);


--
-- TOC entry 3250 (class 2604 OID 21556)
-- Name: tags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags ALTER COLUMN id SET DEFAULT nextval('public.tags_id_seq'::regclass);


--
-- TOC entry 3246 (class 2604 OID 21505)
-- Name: task task_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task ALTER COLUMN task_id SET DEFAULT nextval('public.task_task_id_seq'::regclass);


--
-- TOC entry 3263 (class 2606 OID 21532)
-- Name: contact_in_group contact_in_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_in_group
    ADD CONSTRAINT contact_in_group_pkey PRIMARY KEY (contact_id, group_id);


--
-- TOC entry 3261 (class 2606 OID 21527)
-- Name: contact contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT contact_pkey PRIMARY KEY (id);


--
-- TOC entry 3281 (class 2606 OID 38629)
-- Name: contact_tasksofcontact contact_tasksofcontact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_tasksofcontact
    ADD CONSTRAINT contact_tasksofcontact_pkey PRIMARY KEY (contactsoftask_id, tasksofcontact_task_id);


--
-- TOC entry 3259 (class 2606 OID 21518)
-- Name: group_of_contact group_of_contact_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_of_contact
    ADD CONSTRAINT group_of_contact_pkey PRIMARY KEY (id);


--
-- TOC entry 3252 (class 2606 OID 16565)
-- Name: group group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."group"
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- TOC entry 3279 (class 2606 OID 38624)
-- Name: hibernate_sequences hibernate_sequences_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.hibernate_sequences
    ADD CONSTRAINT hibernate_sequences_pkey PRIMARY KEY (sequence_name);


--
-- TOC entry 3269 (class 2606 OID 21551)
-- Name: mail mail_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail
    ADD CONSTRAINT mail_pkey PRIMARY KEY (email_address, contact_id);


--
-- TOC entry 3267 (class 2606 OID 21544)
-- Name: phone phone_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phone
    ADD CONSTRAINT phone_pkey PRIMARY KEY (phone_number, contact_id);


--
-- TOC entry 3265 (class 2606 OID 21537)
-- Name: subgroups subgroups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subgroups
    ADD CONSTRAINT subgroups_pkey PRIMARY KEY (group_id, subgroup_id);


--
-- TOC entry 3273 (class 2606 OID 21565)
-- Name: tags_in_tasks tags_in_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags_in_tasks
    ADD CONSTRAINT tags_in_tasks_pkey PRIMARY KEY (tags_id, tasks_id);


--
-- TOC entry 3271 (class 2606 OID 21560)
-- Name: tags tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- TOC entry 3283 (class 2606 OID 38679)
-- Name: task_contactsoftask task_contactsoftask_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contactsoftask
    ADD CONSTRAINT task_contactsoftask_pkey PRIMARY KEY (tasksofcontact_task_id, contactsoftask_id);


--
-- TOC entry 3257 (class 2606 OID 21509)
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (task_id);


--
-- TOC entry 3275 (class 2606 OID 30413)
-- Name: task_contact uk_d84vas5ktn4cdefmy4wlqr74c; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contact
    ADD CONSTRAINT uk_d84vas5ktn4cdefmy4wlqr74c UNIQUE (task_id);


--
-- TOC entry 3254 (class 2606 OID 21485)
-- Name: task_sub_tasks uk_jyidv8ywqf8qwmxththchk8yo; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_sub_tasks
    ADD CONSTRAINT uk_jyidv8ywqf8qwmxththchk8yo UNIQUE (sub_tasks_task_id);


--
-- TOC entry 3277 (class 2606 OID 30013)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 3255 (class 1259 OID 21738)
-- Name: fki_task_parent_id_fkey; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_task_parent_id_fkey ON public.task USING btree (parent_id);


--
-- TOC entry 3292 (class 2606 OID 21581)
-- Name: contact_in_group contact_in_group_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_in_group
    ADD CONSTRAINT contact_in_group_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- TOC entry 3293 (class 2606 OID 21586)
-- Name: contact_in_group contact_in_group_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_in_group
    ADD CONSTRAINT contact_in_group_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- TOC entry 3312 (class 2606 OID 38635)
-- Name: contact_tasksofcontact fk3obt561346i5sh393itg5428o; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_tasksofcontact
    ADD CONSTRAINT fk3obt561346i5sh393itg5428o FOREIGN KEY (contactsoftask_id) REFERENCES public.contact(id);


--
-- TOC entry 3303 (class 2606 OID 21661)
-- Name: task_blocking fk3vlanu7y59y40l3p5bsn2iana; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_blocking
    ADD CONSTRAINT fk3vlanu7y59y40l3p5bsn2iana FOREIGN KEY (task_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3308 (class 2606 OID 30419)
-- Name: user_tasksofuser fk4c3duwwlhsa4ll96wvvaem6sm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_tasksofuser
    ADD CONSTRAINT fk4c3duwwlhsa4ll96wvvaem6sm FOREIGN KEY (tasksofuser_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3305 (class 2606 OID 30002)
-- Name: task_contact fk6l81fi07cr6bur767iugw1cm6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contact
    ADD CONSTRAINT fk6l81fi07cr6bur767iugw1cm6 FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- TOC entry 3309 (class 2606 OID 30424)
-- Name: user_tasksofuser fk6yqk73syhi6ph1xbu620r23md; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_tasksofuser
    ADD CONSTRAINT fk6yqk73syhi6ph1xbu620r23md FOREIGN KEY (appusr_id) REFERENCES public."user"(id);


--
-- TOC entry 3290 (class 2606 OID 30022)
-- Name: contact fkb4nh1qoecfprwytk2pb8bh9bi; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT fkb4nh1qoecfprwytk2pb8bh9bi FOREIGN KEY (userofcontacts_id) REFERENCES public."user"(id);


--
-- TOC entry 3304 (class 2606 OID 29997)
-- Name: task_contact fkc9gaeixrt68l5hu3upmqmwvv5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contact
    ADD CONSTRAINT fkc9gaeixrt68l5hu3upmqmwvv5 FOREIGN KEY (task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3289 (class 2606 OID 21707)
-- Name: task fkco85l4ieuyfqlytte9ajftuko; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fkco85l4ieuyfqlytte9ajftuko FOREIGN KEY (parenttask_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3300 (class 2606 OID 21643)
-- Name: userrel fkermu6l7e75epnjki1ejrocmij; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userrel
    ADD CONSTRAINT fkermu6l7e75epnjki1ejrocmij FOREIGN KEY (parentid) REFERENCES public.task(task_id);


--
-- TOC entry 3313 (class 2606 OID 38680)
-- Name: task_contactsoftask fkf6x4lhnbfh1wa8f21sl58hlbh; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contactsoftask
    ADD CONSTRAINT fkf6x4lhnbfh1wa8f21sl58hlbh FOREIGN KEY (contactsoftask_id) REFERENCES public.contact(id);


--
-- TOC entry 3291 (class 2606 OID 38611)
-- Name: contact fki4f47kascj6f3xij3kni3qgwq; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact
    ADD CONSTRAINT fki4f47kascj6f3xij3kni3qgwq FOREIGN KEY (userofcontact_id) REFERENCES public."user"(id);


--
-- TOC entry 3311 (class 2606 OID 38630)
-- Name: contact_tasksofcontact fkkgsitydw66pif8p89iij49mvm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_tasksofcontact
    ADD CONSTRAINT fkkgsitydw66pif8p89iij49mvm FOREIGN KEY (tasksofcontact_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3307 (class 2606 OID 30037)
-- Name: user_contacts fkluq725cqwstraumi6g97h0f22; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contacts
    ADD CONSTRAINT fkluq725cqwstraumi6g97h0f22 FOREIGN KEY (appusr_id) REFERENCES public."user"(id);


--
-- TOC entry 3306 (class 2606 OID 30032)
-- Name: user_contacts fkm7a128umfw4itucrfssjdfcdv; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_contacts
    ADD CONSTRAINT fkm7a128umfw4itucrfssjdfcdv FOREIGN KEY (contacts_id) REFERENCES public.contact(id);


--
-- TOC entry 3310 (class 2606 OID 38640)
-- Name: contact_tasksofcontact fkmr2wrkjse6c1hsbljr99mhkan; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contact_tasksofcontact
    ADD CONSTRAINT fkmr2wrkjse6c1hsbljr99mhkan FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- TOC entry 3302 (class 2606 OID 21656)
-- Name: task_blocking fkprtpj17ds1w2nr113rvba28mx; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_blocking
    ADD CONSTRAINT fkprtpj17ds1w2nr113rvba28mx FOREIGN KEY (blocking_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3286 (class 2606 OID 30027)
-- Name: task fkr8xpom53id0dgngyjv22k27mn; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fkr8xpom53id0dgngyjv22k27mn FOREIGN KEY (useroftasks_id) REFERENCES public."user"(id);


--
-- TOC entry 3285 (class 2606 OID 38606)
-- Name: task fkrx00sxvrom3fpwbcos4mxt6x4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT fkrx00sxvrom3fpwbcos4mxt6x4 FOREIGN KEY (useroftask_id) REFERENCES public."user"(id);


--
-- TOC entry 3314 (class 2606 OID 38685)
-- Name: task_contactsoftask fksir5e08pyii3x6ah5n5mqdkhl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task_contactsoftask
    ADD CONSTRAINT fksir5e08pyii3x6ah5n5mqdkhl FOREIGN KEY (tasksofcontact_task_id) REFERENCES public.task(task_id);


--
-- TOC entry 3301 (class 2606 OID 21648)
-- Name: userrel fktek4x08gyy53mk81gyxjk9a1d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.userrel
    ADD CONSTRAINT fktek4x08gyy53mk81gyxjk9a1d FOREIGN KEY (userid) REFERENCES public.task(task_id);


--
-- TOC entry 3297 (class 2606 OID 21606)
-- Name: mail mail_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail
    ADD CONSTRAINT mail_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- TOC entry 3296 (class 2606 OID 21601)
-- Name: phone phone_contact_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.phone
    ADD CONSTRAINT phone_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES public.contact(id);


--
-- TOC entry 3294 (class 2606 OID 21591)
-- Name: subgroups subgroups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subgroups
    ADD CONSTRAINT subgroups_group_id_fkey FOREIGN KEY (group_id) REFERENCES public."group"(id);


--
-- TOC entry 3295 (class 2606 OID 21596)
-- Name: subgroups subgroups_subgroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subgroups
    ADD CONSTRAINT subgroups_subgroup_id_fkey FOREIGN KEY (subgroup_id) REFERENCES public."group"(id);


--
-- TOC entry 3298 (class 2606 OID 21611)
-- Name: tags_in_tasks tags_in_tasks_tags_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags_in_tasks
    ADD CONSTRAINT tags_in_tasks_tags_id_fkey FOREIGN KEY (tags_id) REFERENCES public.tags(id);


--
-- TOC entry 3299 (class 2606 OID 21616)
-- Name: tags_in_tasks tags_in_tasks_tasks_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tags_in_tasks
    ADD CONSTRAINT tags_in_tasks_tasks_id_fkey FOREIGN KEY (tasks_id) REFERENCES public.task(task_id);


--
-- TOC entry 3288 (class 2606 OID 21576)
-- Name: task task_blocked_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_blocked_by_fkey FOREIGN KEY (blocked_by) REFERENCES public.task(task_id);


--
-- TOC entry 3287 (class 2606 OID 21571)
-- Name: task task_blocking_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_blocking_fkey FOREIGN KEY (blocking) REFERENCES public.task(task_id);


--
-- TOC entry 3284 (class 2606 OID 21733)
-- Name: task task_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.task(task_id) ON DELETE CASCADE NOT VALID;


-- Completed on 2022-10-18 22:50:05

--
-- PostgreSQL database dump complete
--

