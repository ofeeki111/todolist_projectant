CREATE TABLE "task" (
  "id" SERIAL PRIMARY KEY,
  "title" TEXT NOT NULL,
  "description" TEXT NOT NULL,
  "due" DATE,
  "label" TEXT NOT NULL
);

CREATE TABLE "task_blocker" (
  "blocker_of_id" integer,
  "blocked_by_id" integer,
  PRIMARY KEY ("blocker_of_id", "blocked_by_id")
);

CREATE TABLE "subtasks" (
  "task_id" integer,
  "subtask_id" integer,
  "task_info" task NOT NULL,
  PRIMARY KEY ("task_id", "subtask_id")
);

CREATE TABLE "group" (
  "id" SERIAL PRIMARY KEY,
  "name" TEXT
);

CREATE TABLE "contact" (
  "id" SERIAL PRIMARY KEY,
  "first_name" TEXT,
  "last_name" TEXT,
  "nickname" TEXT
);

CREATE TABLE "contact_in_group" (
  "contact_id" INTEGER,
  "group_id" INTEGER,
  PRIMARY KEY ("contact_id", "group_id")
);

CREATE TABLE "subgroups" (
  "group_id" integer,
  "subgroup_id" integer,
  PRIMARY KEY ("group_id", "subgroup_id")
);

CREATE TABLE "phone" (
  "phone_number" TEXT NOT NULL,
  "contact_id" integer,
  PRIMARY KEY ("phone_number", "contact_id")
);

CREATE TABLE "mail" (
  "email_address" TEXT NOT NULL,
  "contact_id" integer,
  PRIMARY KEY ("email_address", "contact_id")
);

CREATE TABLE "tags" (
  "id" SERIAL PRIMARY KEY,
  "title" TEXT
);

CREATE TABLE "tags_in_tasks" (
  "tags_id" INTEGER,
  "tasks_id" INTEGER,
  PRIMARY KEY ("tags_id", "tasks_id")
);

ALTER TABLE "task_blocker" ADD FOREIGN KEY ("blocker_of_id") REFERENCES "task" ("id");

ALTER TABLE "task_blocker" ADD FOREIGN KEY ("blocked_by_id") REFERENCES "task" ("id");

ALTER TABLE "subtasks" ADD FOREIGN KEY ("task_id") REFERENCES "task" ("id");

ALTER TABLE "subtasks" ADD FOREIGN KEY ("subtask_id") REFERENCES "task" ("id");

ALTER TABLE "contact_in_group" ADD FOREIGN KEY ("contact_id") REFERENCES "contact" ("id");

ALTER TABLE "contact_in_group" ADD FOREIGN KEY ("group_id") REFERENCES "group" ("id");

ALTER TABLE "subgroups" ADD FOREIGN KEY ("group_id") REFERENCES "group" ("id");

ALTER TABLE "subgroups" ADD FOREIGN KEY ("subgroup_id") REFERENCES "group" ("id");

ALTER TABLE "phone" ADD FOREIGN KEY ("contact_id") REFERENCES "contact" ("id");

ALTER TABLE "mail" ADD FOREIGN KEY ("contact_id") REFERENCES "contact" ("id");

ALTER TABLE "tags_in_tasks" ADD FOREIGN KEY ("tags_id") REFERENCES "tags" ("id");

ALTER TABLE "tags_in_tasks" ADD FOREIGN KEY ("tasks_id") REFERENCES "task" ("id");
