package com.example.projectant_task_manager.Services.ContactGroupServices;

import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Repositories.ContactRepository;
import com.example.projectant_task_manager.Repositories.TaskRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactService {
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private TaskRepositoryImpl taskRepository;
    //get all tasks that doesn't have parent ID- they are not subtasks
    public List<Contact> getAll(){
        return this.contactRepository.findAll();
    }

    public void addContact(Contact contact){
        this.contactRepository.save(contact);
    }

    public void assignTask(Integer task_id, Integer contact_id) throws NotFoundException {
        Contact contact = contactRepository.findById(contact_id).orElseThrow(NotFoundException::new);
        Task task = taskRepository.findById(task_id).orElseThrow(NotFoundException::new);
        contact.assignTask(task);
        task.assignContact(contact);
        contactRepository.save(contact);
        taskRepository.save(task);
    }

    public void removeContact(Integer contact_id) throws NotFoundException {
        Contact contact = contactRepository.findById(contact_id).orElseThrow(NotFoundException::new);
        for (Task d: contact.getTasksOfContact())
            d.getContactsOfTask().remove(contact);
        contact.getTasksOfContact().removeAll(contact.getTasksOfContact());
        contactRepository.save(contact);
        contactRepository.deleteById(contact_id);
    }
}

