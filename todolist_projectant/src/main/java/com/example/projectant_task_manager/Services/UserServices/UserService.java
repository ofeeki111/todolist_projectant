package com.example.projectant_task_manager.Services.UserServices;

import com.example.projectant_task_manager.Exceptions.NotAuthorizedException;
import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Exceptions.UserAlreadyExistsException;
import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Tags.Tag;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Repositories.AppUsrRepository;
import com.example.projectant_task_manager.Repositories.ContactRepository;
import com.example.projectant_task_manager.Repositories.TagRepository;
import com.example.projectant_task_manager.Repositories.TaskRepositoryImpl;
import com.example.projectant_task_manager.Services.TaskServices.TaskService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.RequiredArgsConstructor;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

@Service @Transactional
public class UserService {
    @Autowired
    AppUsrRepository usrRepository;
    @Autowired
    TaskRepositoryImpl taskRepository;
    @Autowired
    ContactRepository contactRepository;

    @Autowired
    TagRepository tagRepository;

    public List<AppUsr> getAll() {
        return this.usrRepository.findAll();
    }

    public AppUsr createNewUser(ObjectNode usr) throws UserAlreadyExistsException {
        if (usrRepository.findByUsername(usr.get("username").asText()).isPresent()) {
            throw new UserAlreadyExistsException();
        }

        String plainPassword = usr.get("password").asText();
        String encryptedPassword = null;
        try {
            encryptedPassword = encryptPassword(plainPassword);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        AppUsr addedUser = new AppUsr(usr.get("username").asText(), encryptedPassword);
        usrRepository.save(addedUser);

        return addedUser;
    }

    public AppUsr signInUser(ObjectNode usr) throws NotFoundException {
        String username = usr.get("username").asText();
        String password = usr.get("password").asText();

        AppUsr user = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        try {
            if (verifyPassword(password, user.getPassword())) {
                user.setSession_id(AppUsr.generateString());
                return user;
            }
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    private static final String HASH_ALGORITHM = "SHA-256";

    public static String encryptPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(HASH_ALGORITHM);
        byte[] hashBytes = digest.digest(password.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(hashBytes);
    }

    public static boolean verifyPassword(String plainPassword, String hashedPassword) throws NoSuchAlgorithmException {
        String encryptedPassword = encryptPassword(plainPassword);
        return encryptedPassword.equals(hashedPassword);
    }

    private static String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    public AppUsr isSessionAuth(String session_id, String username) throws NotAuthorizedException, NotFoundException {
        AppUsr usr = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        System.out.println(usr.getSession_id() + " ses" + session_id);
        if (usr.getSession_id().equals(session_id)) {
            return usr;
        } else {
            throw new NotAuthorizedException();
        }
    }

    public List<Task> getAllTasksOfUser(String session_id, String username) throws NotAuthorizedException, NotFoundException {
        return isSessionAuth(session_id, username).getTasksOfUser();
    }

    public List<Contact> getAllContactsOfUser(String session_id, String username) throws NotAuthorizedException, NotFoundException {
        return isSessionAuth(session_id, username).getContactsOfUser();
    }

    public void addTaskForUser(String username, Task tsk) throws NotFoundException {
        AppUsr usr = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        tsk.setUserOfTasks(usr);
        taskRepository.save(tsk);
        usrRepository.save(usr);
    }

    public void addContactForUser(String username, Contact contact) throws NotFoundException {
        AppUsr usr = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        contact.setUserOfContact(usr);
        contactRepository.save(contact);
        usrRepository.save(usr);
    }

    public void addTagForUser(String username, Tag tag) throws NotFoundException {
        AppUsr usr = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        tag.setUserOfTag(usr);
        tagRepository.save(tag);
        usrRepository.save(usr);
    }

    public List<Tag> getAllTagsOfUser(String session_id, String username) throws NotFoundException, NotAuthorizedException {
        return isSessionAuth(session_id, username).getTagsOfUser();
    }

    public void deleteTagFromUser(String username, Integer tag_id) throws NotFoundException {
        AppUsr usr = usrRepository.findByUsername(username).orElseThrow(NotFoundException::new);
        Tag tag = tagRepository.findById(tag_id).orElseThrow(NotFoundException::new);
        for (Task t: tag.getTasksOfTags())
            t.getTagsOfTask().remove(tag);
        tag.getTasksOfTags().removeAll(tag.getTasksOfTags());
        tagRepository.deleteById(tag_id);
        usrRepository.save(usr);
    }
}