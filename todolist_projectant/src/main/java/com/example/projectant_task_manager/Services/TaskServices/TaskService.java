package com.example.projectant_task_manager.Services.TaskServices;

import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Tags.Tag;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Repositories.AppUsrRepository;
import com.example.projectant_task_manager.Repositories.ContactRepository;
import com.example.projectant_task_manager.Repositories.TagRepository;
import com.example.projectant_task_manager.Repositories.TaskRepositoryImpl;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Transactional
@Service
@NoArgsConstructor
public class TaskService {
    @Autowired
    @Qualifier("appUsrRepository")
    private AppUsrRepository appUsrRepository;
    @Autowired
    @Qualifier("taskRepositoryImpl")
    private TaskRepositoryImpl taskRepositoryImpl;
    @Autowired
    @Qualifier("contactRepository")
    private  ContactRepository contactRepository;

    @Autowired
    @Qualifier("tagRepository")
    private TagRepository tagRepository;


    //get all tasks that doesn't have parent ID- they are not subtasks
    public List<Task> getAll(){
        return this.taskRepositoryImpl.findByparentTaskIsNull();
    }

    //get a task by id
    public Task getTaskById(Integer id) throws NotFoundException{
        return taskRepositoryImpl.findById(id).orElseThrow(NotFoundException::new);
    }

    public void addTask(Task task){
        this.taskRepositoryImpl.save(task);
    }
    public void updateTask(Task task) throws NotFoundException {
       Task tsk = taskRepositoryImpl.findById(task.getTask_id()).orElseThrow(NotFoundException::new);
       taskRepositoryImpl.save(task);
    }

    public void deleteTask(int id) throws NotFoundException {
        Task tsk = taskRepositoryImpl.findById(id).orElseThrow(NotFoundException::new);
        tsk.getSubTasks().removeAll(tsk.getSubTasks());
        for (Contact d: tsk.getContactsOfTask())
            d.getTasksOfContact().remove(tsk);
        tsk.getContactsOfTask().removeAll(tsk.getContactsOfTask());
        taskRepositoryImpl.save(tsk);
        taskRepositoryImpl.deleteById(id);
    }

    public List<Task> getAllSubtasksOfTask(int id) throws NotFoundException {
        Task tsk = taskRepositoryImpl.findById(id).orElseThrow(NotFoundException::new);
        return tsk.getSubTasks();
    }

    public void addSubTask(Task task, int id) throws NotFoundException {
        Task tsk = getTaskById(id);
        task.setParentTask(tsk);
        taskRepositoryImpl.save(task);
        taskRepositoryImpl.save(tsk);
    }


    public List<Task> getAllBlockingTasksOfATask(int id) throws NotFoundException {
        Task tsk = taskRepositoryImpl.findById(id).orElseThrow(NotFoundException::new);
        return tsk.getBlocking();
    }


    public void blockTask(int id, int blockedTask_id) throws NotFoundException {
        Task tsk = getTaskById(id);
        Task blocked = getTaskById(blockedTask_id);
        tsk.getBlocking().add(blocked);
        taskRepositoryImpl.save(tsk);
        taskRepositoryImpl.save(blocked);
    }


    public void removeTaskFromBlockingList(int id, int blockedTask_id) throws NotFoundException {
        Task tsk_blocked = taskRepositoryImpl.findById(blockedTask_id).orElseThrow(NotFoundException::new);
        taskRepositoryImpl.findById(id).orElseThrow(NotFoundException::new).getBlocking().remove(tsk_blocked);
        taskRepositoryImpl.save(taskRepositoryImpl.findById(id).get());
        taskRepositoryImpl.save(tsk_blocked);
    }

    public void editSubTask(Task subtask) throws NotFoundException {
        if(subtask.getParentTask() != null){
            Task parent = taskRepositoryImpl.findById(subtask.getParentTask().getTask_id()).orElseThrow(NotFoundException::new);
            getTaskById(subtask.getTask_id());
            taskRepositoryImpl.save(subtask);
            taskRepositoryImpl.save(parent);
        }

    }

    public void assignContactToTask(Integer contact_id, Integer task_id) throws NotFoundException {
        Contact contact = contactRepository.findById(contact_id).orElseThrow(NotFoundException::new);
        Task task = taskRepositoryImpl.findById(task_id).orElseThrow(NotFoundException::new);
        contact.assignTask(task);
        taskRepositoryImpl.save(task);
        task.assignContact(contact);
        contactRepository.save(contact);
    }
    public void deleteContactFromTask(Integer contact_id, Integer task_id) throws NotFoundException {
        Contact contact = contactRepository.findById(contact_id).orElseThrow(NotFoundException::new);
        Task task = taskRepositoryImpl.findById(task_id).orElseThrow(NotFoundException::new);

        task.deleteContact(contact);
        contact.deleteTask(task);

        taskRepositoryImpl.save(task);
        contactRepository.save(contact);
    }


    public void addTagToTask(Integer tag_id, Integer task_id) throws NotFoundException {
        Tag tag = tagRepository.findById(tag_id).orElseThrow(NotFoundException::new);
        Task task = taskRepositoryImpl.findById(task_id).orElseThrow(NotFoundException::new);
        System.out.println("tag " + tag.toString());
        System.out.println("task " + task.toString());
        tag.addTask(task);
        task.addTag(tag);
        taskRepositoryImpl.save(task);
        tagRepository.save(tag);
    }

    public void deleteTagFromTask(Integer tag_id, Integer task_id) throws NotFoundException {
        Tag tag = tagRepository.findById(tag_id).orElseThrow(NotFoundException::new);
        Task task = taskRepositoryImpl.findById(task_id).orElseThrow(NotFoundException::new);

        task.getTagsOfTask().remove(tag);
        tag.getTasksOfTags().remove(task);

        taskRepositoryImpl.save(task);
        tagRepository.save(tag);
    }
}

