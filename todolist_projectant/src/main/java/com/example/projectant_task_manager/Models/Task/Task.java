package com.example.projectant_task_manager.Models.Task;
import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Tags.Tag;
import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
@Entity
@Table(name = "task", schema = "public")
@NoArgsConstructor
@ToString
@AllArgsConstructor
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.PropertyGenerator.class,
//        property = "task_id"
//)
public class Task {
    @Id
    @Column(name = "task_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer task_id;

    @JsonProperty
    @Column(name = "title")
    private String title;

    @JsonProperty
    @Column(name = "description")
    private String description;

    public Set<Tag> getTagsOfTask() {
        return tagsOfTask;
    }

    public void setTagsOfTask(Set<Tag> tagsOfTask) {
        this.tagsOfTask = tagsOfTask;
    }

    @JsonProperty
    @Column(name = "is_planned")
    boolean is_planned;

    @JsonProperty
    @Column(name = "due")
    private LocalDateTime due;

    @JsonProperty
    @Column(name = "label")
    private String label;

    @JsonIgnoreProperties({"subTasks",
    "parentTask",
            "title",
            "description",
            "due",
            "label",
            "parentTask",
            "contactsOfTask",
            "planned"})
    @ManyToOne()
    private Task parentTask; // change the name

    @JsonProperty
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "parentTask", orphanRemoval = true)
    private List<Task> subTasks = new ArrayList<>();

    @JsonIgnoreProperties({"password", "session_id","tasksOfUser","contactsOfUser"})
    @JsonProperty
    @ManyToOne
    private AppUsr userOfTasks;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToMany(cascade={CascadeType.DETACH})
    @JoinTable(name="task_blocking",
            joinColumns={@JoinColumn(name="task_task_id")},
            inverseJoinColumns={@JoinColumn(name="blocking_task_id")})
    private List<Task> blocking = new ArrayList<>();

    @Column(name = "contacts_of_tasks")
    @JsonIgnoreProperties({"password", "session_id", "tasksOfContact"})
    @ManyToMany(fetch = FetchType.EAGER,cascade ={CascadeType.MERGE})
    private Set<Contact> contactsOfTask = new HashSet<>();

    @Column(name = "tags_of_tasks")
    @JsonProperty
    @ManyToMany(fetch = FetchType.EAGER,cascade ={CascadeType.MERGE})
    private Set<Tag> tagsOfTask = new HashSet<>();


//---------------------------------------------------------------------------//

public Set<Contact> getContactsOfTask() {
    return contactsOfTask;
}

    public void setContactsOfTask(Set<Contact> contactsOfTask) {
        this.contactsOfTask = contactsOfTask;
    }
    public boolean isIs_planned() {
        return is_planned;
    }

    public void setIs_planned(boolean is_planned) {
        this.is_planned = is_planned;
    }

    public List<Task> getBlocking() {
        return blocking;
    }

    public void setBlocking(List<Task> blocking) {
        this.blocking = blocking;
    }
    public void setTask_id(Integer task_id) {
        this.task_id = task_id;
    }
    public boolean isPlanned() {
        return is_planned;
    }

    public void setPlanned(boolean planned) {
        is_planned = planned;
    }
    public LocalDateTime getDue() {
        return due;
    }

    public void setDue(LocalDateTime due) {
        this.due = due;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Task getParentTask() {
        return parentTask;
    }

    public void setParentTask(Task parentTask) {
        this.parentTask = parentTask;
    }


    public List<Task> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<Task> subTasks) {
        this.subTasks = subTasks;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTask_id() {
        return task_id;
    }

    public void setTask_id(int task_id) {
        this.task_id = task_id;
    }

    public String getDescription() {
        return description;
    }

    public AppUsr getUserOfTasks() {
        return userOfTasks;
    }

    public void setUserOfTasks(AppUsr userOfTasks) {
        this.userOfTasks = userOfTasks;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public void assignContact(Contact contact) {
        contactsOfTask.add(contact);
    }
    public void addTag(Tag tag) {
        tagsOfTask.add(tag);
    }
    public void deleteContact(Contact contact) {
        contactsOfTask.remove(contact);
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", id=" + task_id +
                ", description='" + description + '\'' +
                '}' +
                "contacts " + contactsOfTask.toArray().length;
    }
}
