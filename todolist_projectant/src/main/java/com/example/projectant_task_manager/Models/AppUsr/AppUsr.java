package com.example.projectant_task_manager.Models.AppUsr;

import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Tags.Tag;
import com.example.projectant_task_manager.Models.Task.Task;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Getter
@Setter
@Entity
@EqualsAndHashCode
@Table(name = "user", schema = "public")
public class AppUsr {
    @Id
    @JsonProperty
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @JsonProperty
    @Column(name = "username")
    private String username;

    @JsonProperty
    @Column(name = "password")
    private String password;

    @JsonProperty
    @ReadOnlyProperty
    private String session_id;

    @JsonProperty
    @JsonIgnoreProperties("userOfTasks")
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "userOfTasks",orphanRemoval = true)
    private List<Task> tasksOfUser = new ArrayList<>();

    @JsonIgnoreProperties("userOfContact")
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "userOfContact",orphanRemoval = true)
    private List<Contact> contactsOfUser = new ArrayList<>();

    @JsonProperty
    @OneToMany(fetch=FetchType.EAGER, mappedBy = "userOfTag",orphanRemoval = true)
    private List<Tag> tagsOfUser = new ArrayList<>();

    public AppUsr() {
        this.session_id = generateString();
    }
    public AppUsr(String username, String password) {
        this.username = username;
        this.password = password;
        this.session_id = generateString();
    }
    public void setSession_id() {
        this.session_id = generateString();
    }

    public static String generateString() {
        String uuid = UUID.randomUUID().toString();
        uuid.replace("-", "");
        return uuid;
    }
}
