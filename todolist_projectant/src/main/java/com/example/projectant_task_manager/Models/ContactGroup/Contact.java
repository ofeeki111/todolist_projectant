package com.example.projectant_task_manager.Models.ContactGroup;

import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import com.example.projectant_task_manager.Models.Task.Task;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "contact", schema = "public")

public class Contact {
    @Id
    @JsonProperty
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonProperty
    @Column(name = "first_name")
    private String first_name;

    @JsonProperty
    @Column(name = "last_name")
    private String last_name;


    @JsonProperty
    @Column(name = "nickname")
    private String nickname;

    @JsonIgnoreProperties({"password", "session_id","tasksOfUser","contactsOfUser"})
    @ManyToOne
    private AppUsr userOfContact;

    @Column(name = "tasks_of_contact")
    @JsonIgnoreProperties({"password", "session_id","contactsOfTask"})
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "contactsOfTask")
    private Set<Task> tasksOfContact = new HashSet<>();


    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public AppUsr getUserOfContact() {
        return userOfContact;
    }

    public void setUserOfContact(AppUsr userOfContact) {
        this.userOfContact = userOfContact;
    }

    public Set<Task> getTasksOfContact() {
        return tasksOfContact;
    }

    public void setTasksOfContact(Set<Task> tasksOfContact) {
        this.tasksOfContact = tasksOfContact;
    }

    public void assignTask(Task task) {
        tasksOfContact.add(task);
    }

    public void deleteTask(Task task) {
        tasksOfContact.remove(task);
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
