package com.example.projectant_task_manager.Models.Tags;

import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import com.example.projectant_task_manager.Models.Task.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.boot.logging.log4j2.ColorConverter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@Entity
@Table(name = "tag", schema = "public")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Tag {
    @Id
    @Column(name = "tag_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    @JsonProperty
    private String name;

    @Column(name = "colorOfTag")
    @JsonProperty
    private String colorOfTag = generateColor();

    @Column(name = "tasks_of_tag")
    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER,mappedBy = "tagsOfTask")
    private Set<Task> tasksOfTags = new HashSet<>();

    @ManyToOne
    @JsonIgnore
    private AppUsr userOfTag;


    public String generateColor(){
        // create random object - reuse this as often as possible
        Random random = new Random();

        // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
        int nextInt = random.nextInt(0xffffff + 1);

        // format it as hexadecimal string (with hashtag and leading zeros)W
        String colorCode = String.format("#%06x", nextInt);

        // print it
        return colorCode;
    }
    public void addTask(Task task) {
        tasksOfTags.add(task);
    }
}
