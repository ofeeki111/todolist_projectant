package com.example.projectant_task_manager.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


public class NotFoundException extends Exception{
    public NotFoundException(String message) {
        super(message);
    }
    public NotFoundException(int id) {
        super("Cannot find the object with the ID: " + id);
    }

    public NotFoundException() {

    }
}

