package com.example.projectant_task_manager.Exceptions;

public class UserAlreadyExistsException extends Exception{
    public UserAlreadyExistsException(String message) {
        super(message);
    }
    public UserAlreadyExistsException(int id) {
        super("Cannot find the object with the ID: " + id);
    }

    public UserAlreadyExistsException() {

    }
}

