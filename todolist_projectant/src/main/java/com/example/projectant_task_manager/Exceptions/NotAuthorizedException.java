package com.example.projectant_task_manager.Exceptions;


public class NotAuthorizedException extends Exception{
    public NotAuthorizedException(String message) {
        super(message);
    }
    public NotAuthorizedException() {
        super("Session id not authorized");
    }
}

