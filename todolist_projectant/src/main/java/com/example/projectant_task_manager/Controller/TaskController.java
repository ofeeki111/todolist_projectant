package com.example.projectant_task_manager.Controller;

import com.example.projectant_task_manager.Exceptions.NotAuthorizedException;
import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.Task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.projectant_task_manager.Services.TaskServices.TaskService;

import java.util.List;


@RestController
@CrossOrigin("http://localhost:3000/")
@RequestMapping("/api/tasks")
public class TaskController {
    @Autowired
    private TaskService taskService;
    //get all tasks
    ///URL: api/tasks
    @GetMapping()
    public List<Task> getTasks(){
        return taskService.getAll();
    }

    @GetMapping("{id}")
    public Task getTasksById(@PathVariable Integer id) throws NotFoundException {
        return taskService.getTaskById(id);
    }
    //add a task
    ///URL: api/tasks/add
    @PostMapping("/add")
    public void addTask(@RequestBody Task tsk){
        taskService.addTask(tsk);
    }
//    @PostMapping("/add/{username}")
//    public void addTaskToUser(@PathVariable String username, @RequestBody Task tsk) throws NotAuthorizedException, NotFoundException {
//        taskService.addTaskForUser(username, tsk);
//    }
    //update a task
    ///URL: api/tasks/update
    @PutMapping("/update")
    public void updateTask(@RequestBody Task tsk) throws NotFoundException {
        taskService.updateTask(tsk);
    }
    //delete a task
    ///URL: api/tasks/delete/%id%
    @DeleteMapping("/delete/{id}")
    public void deleteTask(@PathVariable int id) throws NotFoundException{
        taskService.deleteTask(id);
    }
    @PutMapping("/{task_id}/assign/{contact_id}")
    public void assignTaskToContact(@PathVariable Integer task_id, @PathVariable Integer contact_id) throws NotFoundException {
        taskService.assignContactToTask(contact_id, task_id);
    }
    @PutMapping("/{task_id}/remove/{contact_id}")
    public void removeTaskFromContact(@PathVariable Integer task_id, @PathVariable Integer contact_id) throws NotFoundException {
        taskService.deleteContactFromTask(contact_id, task_id);
    }
    @PutMapping("/{task_id}/tags/add/{tag_id}")
    public void addTagToTask(@PathVariable Integer task_id, @PathVariable Integer tag_id) throws NotFoundException {
        taskService.addTagToTask(tag_id, task_id);
    }
    @PutMapping("/{task_id}/tags/remove/{tag_id}")
    public void removeTagFromTask(@PathVariable Integer task_id, @PathVariable Integer tag_id) throws NotFoundException {
        taskService.deleteTagFromTask(tag_id, task_id);
    }
}


