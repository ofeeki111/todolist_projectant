package com.example.projectant_task_manager.Controller;

import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Services.TaskServices.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/tasks/")

//Controller for GET, POST, PUT and DELETE blocked tasks

public class BlockedTaskController {
    @Autowired
    private TaskService taskService;

    // get all the tasks that task {id} id blocking
    /// URL: /api/tasks/{id}/blocking

    @GetMapping("/{id}/blocking")
    public List<Task> getBlockingTasks(@PathVariable int id) throws NotFoundException {
        return taskService.getAllBlockingTasksOfATask(id);
    }


    // Add blocking task to an existing task
    // URL: /api/tasks/{ID OF THE TASK TO ADD TO}/blocking/add/{ID OF THE TASK TO ADD}

    @PostMapping("/{id}/blocking/add/{blockedTask_id}")
    public void addBlockingTask(@PathVariable int id, @PathVariable int blockedTask_id) throws NotFoundException {
        taskService.blockTask(id, blockedTask_id);
    }

    // Remove a task from the blocking list
    // URL: /api/tasks/{ID OF THE TASK TO REMOVE FROM}/blocking/add/{ID OF THE TASK TO REMOVE}

    @DeleteMapping("{id}/delete/{blockedTask_id}")
    public void deleteSubTask(@PathVariable int id, @PathVariable int blockedTask_id) throws NotFoundException {
        taskService.removeTaskFromBlockingList(id, blockedTask_id);
    }
}