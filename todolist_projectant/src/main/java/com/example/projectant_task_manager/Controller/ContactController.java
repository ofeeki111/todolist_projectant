package com.example.projectant_task_manager.Controller;

import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Services.ContactGroupServices.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("http://localhost:3000/contacts")
@RequestMapping("/api/contacts")
public class ContactController {
    @Autowired
    private ContactService contactService;

    @GetMapping()
    public List<Contact> getContacts(){
        return contactService.getAll();
    }

    @PostMapping("/add")
    public void addContact(@RequestBody Contact contact){
        contactService.addContact(contact);
    }

    @DeleteMapping("/{contact_id}/delete")
    public void removeContact(@PathVariable Integer contact_id) throws NotFoundException {
        contactService.removeContact(contact_id);

    }
    @PutMapping("/{contact_id}/assign/{task_id}")
    public void assignTaskToContact(@PathVariable Integer contact_id, @PathVariable Integer task_id) throws NotFoundException {
        contactService.assignTask(contact_id, task_id);
    }
}


