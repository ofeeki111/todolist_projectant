package com.example.projectant_task_manager.Controller;

import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Services.TaskServices.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin("http://localhost:3000/")
@RequestMapping("/api/tasks")
public class SubTaskController {
    @Autowired
    private TaskService taskService;

    //get all subtasks of the specific task with the {id}
    ///URL: /api/tasks/subtasks/{ID}
    @GetMapping("/{id}/subtasks")
    public List<Task> getSubTasks(@PathVariable int id) throws NotFoundException {
        return taskService.getAllSubtasksOfTask(id);
    }

    // Add a task to a subtasks list of a specific task
    // URL: /api/tasks/subtasks/{ID OF THE TASK TO ADD TO}/add
    @PostMapping("/{id}/subtasks/add")
    public void addSubTask(@RequestBody Task task, @PathVariable int id) throws NotFoundException {
        taskService.addSubTask(task, id);
    }

    // Add a task to a subtasks list of a specific task
    // URL: /api/tasks/subtasks/{ID OF THE TASK TO ADD TO}/add
    @PutMapping("/subtasks/edit")
    public void editSubTask(@RequestBody Task subtask) throws NotFoundException {
        taskService.editSubTask(subtask);
    }


    // Remove a task from the subtasks list
    // URL: /api/tasks/subtasks/delete/{ID OF THE TASK TO REMOVE FROM}/{ID OF THE SUBTASK TO REMOVE}/
    @DeleteMapping("/{parent_id}/subtasks/{id}/delete/")
    public void deleteSubTask( @PathVariable int id) throws NotFoundException {
        taskService.deleteTask(id);
    }
}