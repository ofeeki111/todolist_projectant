package com.example.projectant_task_manager.Controller;

import com.example.projectant_task_manager.Exceptions.NotAuthorizedException;
import com.example.projectant_task_manager.Exceptions.NotFoundException;
import com.example.projectant_task_manager.Exceptions.UserAlreadyExistsException;
import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Tags.Tag;
import com.example.projectant_task_manager.Models.Task.Task;
import com.example.projectant_task_manager.Services.ContactGroupServices.ContactService;
import com.example.projectant_task_manager.Services.UserServices.UserService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService usrService;

    @GetMapping()
    public List<AppUsr> getUser(){
        return usrService.getAll();
    }

    @PostMapping("/login")
    @ResponseBody
    public AppUsr signInUser(@RequestBody ObjectNode usr) throws NotFoundException {
        return usrService.signInUser(usr);
    }

    @PostMapping("/register")
    @ResponseBody
    public AppUsr addUser(@RequestBody ObjectNode usr) throws UserAlreadyExistsException, NotFoundException {
         return usrService.createNewUser(usr);
    }

    @GetMapping("/{username}/tasks")
    public List<Task> getTasksOfUser(@RequestHeader("session_id") String session_id, @PathVariable String username) throws NotAuthorizedException, NotFoundException {
        return usrService.getAllTasksOfUser(session_id, username);
    }

    @GetMapping("/{username}/contacts")
    public List<Contact> getContactsOfUser(@RequestHeader("session_id") String session_id, @PathVariable String username) throws NotAuthorizedException, NotFoundException {
        return usrService.getAllContactsOfUser(session_id, username);
    }

    @GetMapping("/{username}/tags")
    public List<Tag> getTagsOfUser(@RequestHeader("session_id") String session_id, @PathVariable String username) throws NotAuthorizedException, NotFoundException {
        return usrService.getAllTagsOfUser(session_id, username);
    }

    @PostMapping("/{username}/tasks/add")
    public void addTaskToUser(@PathVariable String username, @RequestBody Task tsk) throws NotAuthorizedException, NotFoundException {
        usrService.addTaskForUser(username, tsk);
    }

    @PostMapping("/{username}/contacts/add")
    public void addContactToUser(@PathVariable String username, @RequestBody Contact contact) throws NotAuthorizedException, NotFoundException {
        usrService.addContactForUser(username, contact);
    }
    @PostMapping("/{username}/tags/add")
    public void addTagToUser(@PathVariable String username, @RequestBody Tag tag) throws NotAuthorizedException, NotFoundException {
        usrService.addTagForUser(username, tag);
    }

    @DeleteMapping("/{username}/tags/{id}/delete")
    public void deleteTagFromUser(@PathVariable String username, @PathVariable Integer id) throws NotAuthorizedException, NotFoundException {
        usrService.deleteTagFromUser(username, id);
    }
}
