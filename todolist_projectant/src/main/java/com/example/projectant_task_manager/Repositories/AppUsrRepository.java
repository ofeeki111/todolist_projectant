package com.example.projectant_task_manager.Repositories;

import com.example.projectant_task_manager.Models.AppUsr.AppUsr;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppUsrRepository extends JpaRepository<AppUsr,Long> {
    Optional<AppUsr> findByUsername(String username);
    Optional<AppUsr> findByPassword(String password);
}
