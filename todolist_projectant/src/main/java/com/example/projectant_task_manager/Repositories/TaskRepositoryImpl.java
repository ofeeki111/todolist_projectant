package com.example.projectant_task_manager.Repositories;
import com.example.projectant_task_manager.Models.Task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TaskRepositoryImpl extends JpaRepository<Task,Integer> {
    //Staff findByEmailAndPasswordAndInstId(String email, String password, Integer id);
    List<Task> findByparentTaskIsNull();
}
