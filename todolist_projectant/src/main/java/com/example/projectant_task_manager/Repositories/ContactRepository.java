package com.example.projectant_task_manager.Repositories;

import com.example.projectant_task_manager.Models.ContactGroup.Contact;
import com.example.projectant_task_manager.Models.Task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact,Integer> {
}
