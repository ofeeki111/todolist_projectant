package com.example.projectant_task_manager.Repositories;

import com.example.projectant_task_manager.Models.Tags.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag,Integer> {
}
